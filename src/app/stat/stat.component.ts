import { Component, Input,
  ChangeDetectionStrategy,
  ChangeDetectorRef, OnInit, ViewChild, ElementRef} from '@angular/core';
  import { element, promise } from 'protractor';
import { async } from '@angular/core/testing';
import { GestionagenceService } from '../services/gestionagence.service';
import { ProdsouscrireService } from '../services/prodsouscrire.service';
import { GestionuserService } from '../services/gestionuser.service';
import { ProduitService } from '../services/produit.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestiontypeprodService } from '../services/gestiontypeprod.service';
import { GestionclientService } from '../services/gestionclient.service';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { GestioncreditService } from '../services/gestioncredit.service';
import { SouscriptionService } from '../services/souscription.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';



@Component({
  selector: 'app-stat',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './stat.component.html',
  styleUrls: ['./stat.component.css']
})
export class StatComponent implements OnInit {
  d1:any
  d2:any
  
  cat:any=[]
  @Input() myData = [];
  x:any
  myType = 'PieChart';
  titre:string="Souscription par categorie client"
  width:string="800"
     height:string="500"
     user:any
     cars:any=[]
     id:any
     @Input() testdelay : Boolean = false

  constructor(private cd: ChangeDetectorRef,private serv8: GestionagenceService, private serv7: GestionuserService, private serv6: ProdsouscrireService, private serv5: ProduitService, private serv4: GestioncaracterstiqueService, private serv: GestioncategorieService, private serv3: GestiontypeprodService, private serv2: GestionclientService, private local: LocalStorageService, private route: Router,private cr :GestioncreditService,private sous:SouscriptionService) { }

  ngOnInit() {
    this.getallcat()
    this.getchartdata()
   
  this.getuser()
  this.getallcarac()
  setTimeout( ()=>{
    this.update();
}, 500);
 
  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
  deconnexion() {
    this.local.clear("token")
    this.route.navigate(['/login'])
}
getuser() {
  this.serv7.getrole().subscribe(
      data => {
          this.user = data;
         // console.log('exected' + data);
      },
      err => {
         // console.log(err);
      }
  );

}


update() {
  this.delayedTest()
  this.cd.detectChanges();
}

delayedTest(){
 
  this.testdelay=true;
  this.cd.markForCheck();
}
  getallcat() {
    this.serv.getallcat().subscribe(
        data => {
           
            this.cat = data;
         
            this.getchartdata()
            
        },
        err => {
            console.log(err);
        }
    );

}
async  getchartdata(){
           
  this.cat.forEach(async element => {
      this.x = await this.getsouscriptionbycat(element.id);
      this.myData.push([element.nom , this.x.length])
  });
  console.log(this.myData)
  this.cd.markForCheck();
}
getsouscriptionbycat(id:any){
  return new Promise(resolve => {
    this.sous.getsousbycatanddate(id,this.d1,this.d2).subscribe(
        data => {
            resolve(data);
            
            
            //console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );
 })
}

}
