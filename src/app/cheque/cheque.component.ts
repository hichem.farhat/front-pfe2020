import { Component, OnInit } from '@angular/core';
import { GestionchequeService } from '../services/gestioncheque.service';
import { GestionuserService } from '../services/gestionuser.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { Cheque } from '../cheque';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';

@Component({
  selector: 'app-cheque',
  templateUrl: './cheque.component.html',
  styleUrls: ['./cheque.component.css']
})
export class ChequeComponent implements OnInit {
  user:any
  cheque:any=[]
  ch:Cheque=new Cheque()
  count:any
  cat:any=[]
  cars:any=[]
  searchtext:any
  p:number=1

  constructor(private serv1:GestioncategorieService,private serv4:GestioncaracterstiqueService,private serv:GestionchequeService,private serv2:GestionuserService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }

  ngOnInit() {
    this.getcount()
    this.getuser()
    this.getallcheque()
    this.getallcarac()
    this.getALLcAt()
  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
  getALLcAt(){
    this.serv1.getallcat().subscribe(
      data => {
        this.cat = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
  getuser(){
    this.serv2.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcheque(){
    this.serv.getallcheque().subscribe(
      data=>{this.cheque=data},
  erre=>{}

    )
  }
  add()
      {
      
      this.serv.addcheque(this.ch).subscribe(
  
        data=>{this.route.navigate(['/cheque'])
      this.ngOnInit()},
        erre=>{}
      )
    
    
    
    }
  supprimer(id:number){
    if (window.confirm('Are sure you want to delete this Abonnee ?')){
    this.serv.deletecheque(id).subscribe(
      data => {
        console.log(data)
        this.toastr.success(' data successfully deleted!')
        this.ngOnInit() },
      err => { })
  
      }
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
    getcount(){
      this.serv.getcount().subscribe(
        data=>{
          this.count=data;console.log('exected' +data);
        },
        err=>{
          console.log(err);
        }
      );
  
    }

}
