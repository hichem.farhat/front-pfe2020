import { Component, OnInit } from '@angular/core';
import { GestionService } from '../gestion.service';
import { Typeprod } from '../typeprod';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestiontypeprodService } from '../services/gestiontypeprod.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import * as $ from 'jquery';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
@Component({
  selector: 'app-typeproduits',
  templateUrl: './typeproduits.component.html',
  styleUrls: ['./typeproduits.component.css']
})
export class TypeproduitsComponent implements OnInit {
type:any=[]
pdf:any
@LocalStorage() token:any;
typeprod:Typeprod= new Typeprod();
cat:any=[]
cars:any=[]
user:any
searchtext:any
p:number=1

  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private serv:GestiontypeprodService,private route:Router,private toastr: ToastrService,private local:LocalStorageService, private http:HttpClient) {  }

  ngOnInit() {
    this.gettype()
    this.getuser()
    this.getALLcAt()
    this.getallcarac()
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  supprimer(id:number){
    if (window.confirm('Are sure you want to delete this type ?')){
    this.serv.deletetype(id).subscribe(
      data => {
        console.log(data)
        this.toastr.success(' data successfully deleted!')
        this.ngOnInit() },
      err => { })
  
      }
    }
  gettype(){
    this.serv.getalltypeprod().subscribe(
      data => {
        this.type = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
    add()
    {
    
    this.serv.addtype(this.typeprod).subscribe(

      data=>{this.route.navigate(['/type'])
    this.ngOnInit()},
      erre=>{}
    )

}
generate(){
  console.log(localStorage.getItem("token"));
$.ajax({
  xhrFields: {
      withCredentials: true
  },
  beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', localStorage.getItem("token"));
  },
  url:"http://127.0.0.1:8080/typeprod/pdf"
});
  /*console.log(localStorage.getItem("token"));
  console.log("dddddd");
  let headers = new HttpHeaders();
headers = headers.set('authorization',localStorage.getItem("token"));
return this.http.get("http://127.0.0.1:8080/typeprod/pdf", { headers: headers});
this.route.navigateByUrl("",headers);*/
  }



deconnexion()
{
  this.local.clear("token")
  this.route.navigate(['/login'])
}}
