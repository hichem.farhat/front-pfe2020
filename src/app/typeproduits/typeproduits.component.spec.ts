import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeproduitsComponent } from './typeproduits.component';

describe('TypeproduitsComponent', () => {
  let component: TypeproduitsComponent;
  let fixture: ComponentFixture<TypeproduitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeproduitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeproduitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
