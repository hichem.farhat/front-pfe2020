import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddtpComponent } from './addtp.component';

describe('AddtpComponent', () => {
  let component: AddtpComponent;
  let fixture: ComponentFixture<AddtpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddtpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddtpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
