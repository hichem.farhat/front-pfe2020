import { Component, OnInit } from '@angular/core';
import { GestionuserService } from 'src/app/services/gestionuser.service';
import { GestioncategorieService } from 'src/app/services/gestioncategorie.service';
import { GestioncaracterstiqueService } from 'src/app/services/gestioncaracterstique.service';
import { GestiontypeprodService } from 'src/app/services/gestiontypeprod.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { HttpClient } from '@angular/common/http';
import { Typeprod } from 'src/app/typeprod';

@Component({
  selector: 'app-addtp',
  templateUrl: './addtp.component.html',
  styleUrls: ['./addtp.component.css']
})
export class AddtpComponent implements OnInit {

 
pdf:any
@LocalStorage() token:any;
typeprod:Typeprod= new Typeprod();
cat:any=[]
cars:any=[]
user:any


  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private serv:GestiontypeprodService,private route:Router,private toastr: ToastrService,private local:LocalStorageService, private http:HttpClient) {  }

  ngOnInit() {
    
    this.getuser()
    this.getALLcAt()
    this.getallcarac()
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  supprimer(id:number){
    if (window.confirm('Are sure you want to delete this type ?')){
    this.serv.deletetype(id).subscribe(
      data => {
        console.log(data)
        this.toastr.success(' data successfully deleted!')
        this.ngOnInit() },
      err => { })
  
      }
    }
 
    add()
    {
    
    this.serv.addtype(this.typeprod).subscribe(

      data=>{this.route.navigate(['/type'])
    this.ngOnInit()},
      erre=>{}
    )

}
generate(){
  console.log(localStorage.getItem("token"));
$.ajax({
  xhrFields: {
      withCredentials: true
  },
  beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', localStorage.getItem("token"));
  },
  url:"http://127.0.0.1:8080/typeprod/pdf"
});
  /*console.log(localStorage.getItem("token"));
  console.log("dddddd");
  let headers = new HttpHeaders();
headers = headers.set('authorization',localStorage.getItem("token"));
return this.http.get("http://127.0.0.1:8080/typeprod/pdf", { headers: headers});
this.route.navigateByUrl("",headers);*/
  }



deconnexion()
{
  this.local.clear("token")
  this.route.navigate(['/login'])
}}
