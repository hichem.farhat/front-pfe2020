import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestionagenceService {

  constructor(private http:HttpClient) { }
  getallagence(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/ag/agences",{headers:headers})
  }
  addag(ag:any){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.post("http://127.0.0.1:8080/ag/save",ag,{headers:headers})
  }
  deleteag(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.delete("http://127.0.0.1:8080/ag/delag/"+id,{headers:headers})
  }
  getagbyid(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get('http://127.0.0.1:8080/ag/ag/'+id,{headers:headers})

  }
  updateag(id: number,data){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.put("http://127.0.0.1:8080/ag/updateag/"+id,data,{headers:headers})
  }
  getcount(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get('http://127.0.0.1:8080/ag/count',{headers:headers})

  }

}
