import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestioncaracterstiqueService {

  constructor(private http:HttpClient) { }
  getallcarac(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/carac/cars",{headers:headers})
  }deletecarac(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.delete("http://127.0.0.1:8080/carac/delca/"+id,{headers:headers})

  }
  add(carac:any){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.post("http://127.0.0.1:8080/carac/save",carac,{headers:headers})

  }
  getcaracbyid(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/carac/ca/"+id,{headers:headers})





  }
  updatecarac(id: number,data){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.put("http://127.0.0.1:8080/carac/updatecarac/"+id,data,{headers:headers})

  }
  countcarac(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/carac/count",{headers:headers})

  }

}
