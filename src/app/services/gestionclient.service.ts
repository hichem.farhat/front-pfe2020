import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestionclientService {

  constructor(private http:HttpClient) { }
  getallclientbycat(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/clt/cltcat/"+id,{headers:headers})
  }
  getallclt(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/clt/clients",{headers:headers})
    
    }
    delclt(id:number){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.delete('http://127.0.0.1:8080/clt/delclt/'+id,{headers:headers})
    
    
    }
    getcltbyid(id:number){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/clt/clt/"+id,{headers:headers})
    }
    updateclt(id: number,data){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.put("http://127.0.0.1:8080/clt/updateclt/"+id,data,{headers:headers})
    }
    addclt(clt:any){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.post("http://127.0.0.1:8080/clt/addclt",clt,{headers:headers})
    }
    countclt(){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/clt/count",{headers:headers})
    }
    getclientbyagence(id:number){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/clt/cltag/"+id,{headers:headers})

    }
    getpourcentage(){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/clt/ff/",{headers:headers})

    } getmeilleur(){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/clt/test/",{headers:headers})

    }
    getcountag(id:number){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/clt/counttag/"+id,{headers:headers})

    }
    getmeilleur2(id:any){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/clt/test2/"+id,{headers:headers})}
      getpourcentage2(id:number){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.get("http://127.0.0.1:8080/clt/ff2/"+id,{headers:headers})}
  
}
