import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestiontypeprodService {

  constructor(private http:HttpClient) { }
  getalltypeprod(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})

    return this.http.get("http://127.0.0.1:8080/typeprod/Typeprod",{headers:headers})
  }
  addtype(type:any){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.post("http://127.0.0.1:8080/typeprod/addtype",type,{headers:headers})
  
  }
  deletetype(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
  
    return this.http.delete('http://127.0.0.1:8080/typeprod/deltype/'+id,{headers:headers})
  }
  gettypebyid(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get('http://127.0.0.1:8080/typeprod/typeprod/'+id,{headers:headers})
  }
  updatetype(id: number,data){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.put('http://127.0.0.1:8080/typeprod/typeprod/'+id,data,{headers:headers});
  }
  getcounttypeprod(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/typeprod/count",{headers:headers})
  
  }
  generate(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/typeprod/pdf",{headers:headers})

  }

}
