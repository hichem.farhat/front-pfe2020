import { TestBed } from '@angular/core/testing';

import { GestioncarteService } from './gestioncarte.service';

describe('GestioncarteService', () => {
  let service: GestioncarteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestioncarteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
