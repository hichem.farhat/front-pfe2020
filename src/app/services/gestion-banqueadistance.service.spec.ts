import { TestBed } from '@angular/core/testing';

import { GestionBanqueadistanceService } from './gestion-banqueadistance.service';

describe('GestionBanqueadistanceService', () => {
  let service: GestionBanqueadistanceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestionBanqueadistanceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
