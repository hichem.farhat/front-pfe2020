import { TestBed } from '@angular/core/testing';

import { ProdsouscrireService } from './prodsouscrire.service';

describe('ProdsouscrireService', () => {
  let service: ProdsouscrireService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProdsouscrireService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
