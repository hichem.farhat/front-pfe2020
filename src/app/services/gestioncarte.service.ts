import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestioncarteService {

  constructor(private http:HttpClient) {}
    getallcarte(){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/carte/cartes",{headers:headers})
    }
    addcarte(carte:any){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.post("http://127.0.0.1:8080/carte/addcarte",carte,{headers:headers})
    }
    deletecarte(id:number){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.delete("http://127.0.0.1:8080/carte/delcarte/"+id,{headers:headers})
    }
    getlcount(){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/carte/count",{headers:headers})
    } getallcartebyagence(id:number){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/carte/carteag/"+id,{headers:headers})
    }
    countag(id:number){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/carte/countag/"+id,{headers:headers})
    }
    
   
}
