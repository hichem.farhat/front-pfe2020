import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestionCompteépargneService {

  constructor(private http:HttpClient) { }
  getallCompteépargne(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/ce/getall",{headers:headers})
  }
 
  deleteCompteépargne(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.delete("http://127.0.0.1:8080/ce/delce/"+id,{headers:headers})
  }
  count(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/ce/count/",{headers:headers})
  }
  getallCompteepargnebyagence(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/ce/ceag/"+id,{headers:headers})
  }
  countag(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/ce/countag/"+id,{headers:headers})
  }
}
