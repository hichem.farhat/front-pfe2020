import { TestBed } from '@angular/core/testing';

import { GestioncreditService } from './gestioncredit.service';

describe('GestioncreditService', () => {
  let service: GestioncreditService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestioncreditService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
