import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestionchequeService {

  constructor(private http:HttpClient) { }
  getallcheque(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/cheque/cheques",{headers:headers})
  }
  addcheque(cheque:any){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.post("http://127.0.0.1:8080/cheque/addcheque",cheque,{headers:headers})
  }
  deletecheque(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.delete("http://127.0.0.1:8080/cheque/delcheque/"+id,{headers:headers})
  }
  getcount(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/cheque/count/",{headers:headers})

  }
  getallchequebyagence(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/cheque/chequeag/"+id,{headers:headers})
  }
  countag(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/cheque/countag/"+id,{headers:headers})
  }
}
