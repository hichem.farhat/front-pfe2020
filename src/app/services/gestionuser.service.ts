import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from "@auth0/angular-jwt";
@Injectable({
  providedIn: 'root'
})
export class GestionuserService {
  username:string;
  token:string;
  id:any

  constructor(private http:HttpClient) { 
    
  }
  login(request){
    return this.http.post("http://127.0.0.1:8080/auth/login",request,{observe:'response'})
    }
    saveToken(token){
      let helper = new JwtHelperService();
      this.token=token;
      let decodedToken = helper.decodeToken(token);
      this.username=decodedToken.sub;
      
      
  
      // recuperer le username
      }
      getuser(){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.get("http://127.0.0.1:8080/auth/getall/",{headers:headers})
  
      }
      getrole(){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.get("http://127.0.0.1:8080/auth/user/",{headers:headers})
      }
      save(user:any){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.post("http://127.0.0.1:8080/auth/save/",user,{headers:headers})

      }
      deleteuser(id:number){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.delete("http://127.0.0.1:8080/auth/deleteuser/"+id,{headers:headers})

      }
      getuserr(id:number){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.get("http://127.0.0.1:8080/auth/getuser/"+id,{headers:headers})

      }
      updateuser(id:number,data){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.put("http://127.0.0.1:8080/auth/updateuser/"+id,data,{headers:headers})

      }
      UPDATE(id:number,user:any){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.put("http://127.0.0.1:8080/auth/update/"+id,user,{headers:headers})

      }
      updateprofile(u:any){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.post("http://127.0.0.1:8080/auth/updateprofileee/",u,{headers:headers})}
        updateass(u:any){
          let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
          return this.http.post("http://127.0.0.1:8080/auth/updatepass/",u,{headers:headers})}

}
