import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  constructor(private http:HttpClient) { }
  getallprod(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/prod/prods",{headers:headers})
  }
  addprod(produit:any){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.post("http://127.0.0.1:8080/prod/addprod",produit,{headers:headers})
  }
  deleteprod(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.delete("http://127.0.0.1:8080/prod/delprod/"+id,{headers:headers})
  }
  getprodid(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/prod/getprod/"+id,{headers:headers})
  }
  updateprod(id:number,data){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.put("http://127.0.0.1:8080/prod/updateprod/"+id,data,{headers:headers})
  }
  getprodbyx(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/prod/aze/"+id,{headers:headers})
  }
  count(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/prod/count/",{headers:headers})

  }
  meilleulprod(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/prod/test/",{headers:headers})

  }
  
}
