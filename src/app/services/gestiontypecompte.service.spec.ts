import { TestBed } from '@angular/core/testing';

import { GestiontypecompteService } from './gestiontypecompte.service';

describe('GestiontypecompteService', () => {
  let service: GestiontypecompteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestiontypecompteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
