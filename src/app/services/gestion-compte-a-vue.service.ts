import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestionCompteAVueService {

  constructor(private http:HttpClient) { }
  getallCompteAVue(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/cav/all",{headers:headers})
  }
 
  deleteCompteAVue(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.delete("http://127.0.0.1:8080/cav/delcav/"+id,{headers:headers})
  }
  count(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/cav/count/",{headers:headers})
  }
  getallCompteAVuebyagence(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/cav/cavag/"+id,{headers:headers})
  }
  countag(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/cav/countag/"+id,{headers:headers})
  }
}
