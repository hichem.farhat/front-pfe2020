import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestiontypecompteService {

  constructor(private http:HttpClient) { 

  }
  getalltypecompte(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/typecompte/typecp",{headers:headers})
  
  }
  savetypecp(type:any){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return  this.http.post("http://127.0.0.1:8080/typecompte/save",type,{headers:headers})
  }
  deletetypecp(id :number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.delete("http://127.0.0.1:8080/typecompte/delctype/"+id,{headers:headers})
  }
  gettypecompte(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/typecompte/gettype/"+id,{headers:headers})
  }
  updatetcp(id:number,data){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.put("http://127.0.0.1:8080/typecompte/updatetcp/"+id,data,{headers:headers})
  }
}
