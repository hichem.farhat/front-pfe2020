import { TestBed } from '@angular/core/testing';

import { GestioncompteService } from './gestioncompte.service';

describe('GestioncompteService', () => {
  let service: GestioncompteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestioncompteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
