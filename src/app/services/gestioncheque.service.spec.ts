import { TestBed } from '@angular/core/testing';

import { GestionchequeService } from './gestioncheque.service';

describe('GestionchequeService', () => {
  let service: GestionchequeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestionchequeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
