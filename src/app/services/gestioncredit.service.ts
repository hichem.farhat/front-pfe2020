import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestioncreditService {

  constructor(private http:HttpClient) { }
  getallcredit(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/credit/Credits/",{headers:headers})
  }
  delcredit(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.delete("http://127.0.0.1:8080/credit/delcredit/"+id,{headers:headers})
  }
  savecredit(credit:any){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.post("http://127.0.0.1:8080/credit/save/",credit,{headers:headers})
  
  }
  validation (id: number,data){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.put('http://127.0.0.1:8080/credit/creditv/'+id,data,{headers:headers});
  }
  update(id:number,data){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.put('http://127.0.0.1:8080/credit/update/'+id,data,{headers:headers});
  }
  getcreditbyid(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get('http://127.0.0.1:8080/credit/cred/'+id,{headers:headers});}
    getcreditui(){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get('http://127.0.0.1:8080/credit/oui/',{headers:headers});}

    
    getcreditnon(){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get('http://127.0.0.1:8080/credit/non/',{headers:headers});}
      getpourcent1(){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.get('http://127.0.0.1:8080/credit/p1/',{headers:headers});

      }
      getpourcent2(){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.get('http://127.0.0.1:8080/credit/p2/',{headers:headers});

      }
      getcreditbyagence(id:number){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.get('http://127.0.0.1:8080/credit/getcreditbyag/'+id,{headers:headers});

      }
      getcount(){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.get('http://127.0.0.1:8080/credit/count/',{headers:headers});

      }
      getcountag(id:number){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.get('http://127.0.0.1:8080/credit/countag/'+id,{headers:headers});

      }
      getpourcent11(id:number){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.get('http://127.0.0.1:8080/credit/pp1/'+id,{headers:headers});

      }
      getpourcent22(id:number){
        let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
        return this.http.get('http://127.0.0.1:8080/credit/pp2/'+id,{headers:headers});

      }

    
}
