import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestionBanqueadistanceService {

  constructor(private http:HttpClient) { }
    getallbanqueadistance(){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/bad/getall",{headers:headers})
    }
   
    deletebad(id:number){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.delete("http://127.0.0.1:8080/bad/delbad/"+id,{headers:headers})
    }
    count(){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/bad/count/",{headers:headers})
    }
    getallbanqueadistancebyagence(id:number){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/bad/badag/"+id,{headers:headers})
    }
    countag(id:number){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/bad/countag/"+id,{headers:headers})
    }
    



  
}
