import { TestBed } from '@angular/core/testing';

import { GestionCompteAVueService } from './gestion-compte-a-vue.service';

describe('GestionCompteAVueService', () => {
  let service: GestionCompteAVueService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestionCompteAVueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
