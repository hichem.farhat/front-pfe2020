import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestioncompteService {

  constructor(private http:HttpClient) { }
  getaccountbyid(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/compte/getcompte/"+id,{headers:headers})
  }
  updatecompte(id:number,data){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.put("http://127.0.0.1:8080/compte/updatecp/"+id,data,{headers:headers})
  }
  getallcomptes(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/compte/comptes",{headers:headers})
  }
  deleteaccount(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.delete("http://127.0.0.1:8080/compte/delcompte/"+id,{headers:headers})
  }
  addaccount(compte:any){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.post("http://127.0.0.1:8080/compte/save",compte,{headers:headers})
  
  }
  getallcomptebyagence(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/compte/getallcpag/"+id,{headers:headers})
  }
}
