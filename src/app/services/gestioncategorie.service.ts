import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestioncategorieService {

  constructor(private http:HttpClient) { }
  getallcat(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})

    return this.http.get("http://127.0.0.1:8080/cat/allcats",{headers:headers})
  }
  updatecat(id: number,data){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.put("http://127.0.0.1:8080/cat/updatecat/"+id,data,{headers:headers})
  
  }
  getcatbyid(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/cat/cat/"+id,{headers:headers})
  }
  addcats(cat:any){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.post("http://127.0.0.1:8080/cat/addcat",cat,{headers:headers})
  }
  deletecat(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})


    return this.http.delete('http://127.0.0.1:8080/cat/delcat/'+id,{headers:headers})
  }
}
