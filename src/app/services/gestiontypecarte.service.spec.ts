import { TestBed } from '@angular/core/testing';

import { GestiontypecarteService } from './gestiontypecarte.service';

describe('GestiontypecarteService', () => {
  let service: GestiontypecarteService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestiontypecarteService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
