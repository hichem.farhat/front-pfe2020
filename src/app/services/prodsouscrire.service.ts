import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProdsouscrireService {

  constructor(private http:HttpClient) {

   }
   getallprodsous(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/prodsous/prodsous",{headers:headers})
  }
  saveprodsous(prodsous:any){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.post("http://127.0.0.1:8080/prodsous/save",prodsous,{headers:headers})
  
  }
  delprodsous(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.delete("http://127.0.0.1:8080/prodsous/delprodsous/"+id,{headers:headers})
    
  }
  getprodsous(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/prodsous/getprodsous/"+id,{headers:headers})
  }
  updateprodsouss(id:number,data){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.put("http://127.0.0.1:8080/prodsous/updateprodsous/"+id,data,{headers:headers})
  }
  pourcentage(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/prodsous/ff/",{headers:headers})

  }
  getcount(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/prodsous/count/",{headers:headers})

  }
  getallprodsousbyag(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/prodsous/psag/"+id,{headers:headers})


  }
  pourcentage2(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/prodsous/ff2/"+id,{headers:headers})

  }
  countprodsousbyag(id :number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/prodsous/couuntag/"+id,{headers:headers})
  }
}
