import { TestBed } from '@angular/core/testing';

import { GestionagenceService } from './gestionagence.service';

describe('GestionagenceService', () => {
  let service: GestionagenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestionagenceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
