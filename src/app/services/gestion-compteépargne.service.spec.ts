import { TestBed } from '@angular/core/testing';

import { GestionCompteépargneService } from './gestion-compteépargne.service';

describe('GestionCompteépargneService', () => {
  let service: GestionCompteépargneService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestionCompteépargneService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
