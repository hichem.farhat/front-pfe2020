import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class SouscriptionService {

  constructor(private http:HttpClient) {

   }
   getallsous(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/sous/souss",{headers:headers})
  
  }
  Savesous(Souscription:any){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})

    return this.http.post("http://127.0.0.1:8080/sous/add",Souscription,{headers:headers})
  
  
  }
  delsous(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.delete("http://127.0.0.1:8080/sous/delsous/"+id,{headers:headers})
    
  }
  getsous(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/sous/getsous/"+id,{headers:headers})
 
   }
   getcount(){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/sous/count/",{headers:headers})
 
   }
   updatesouss(id:number,data){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
     return this.http.put("http://127.0.0.1:8080/sous/updatetsous/"+id,data,{headers:headers})
   }
   getallsousbyagence(id:number){
    let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
    return this.http.get("http://127.0.0.1:8080/sous/sousag/"+id,{headers:headers})}
    getallsousbycat(id:number){
      let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
      return this.http.get("http://127.0.0.1:8080/sous/getsousbycat/"+id,{headers:headers})

}
getsouscriptionbydate(date1:any,date2:any){
  let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
   return this.http.get("http://127.0.0.1:8080/sous/getsouscriptionbydate/"+date1+"/"+date2,{headers:headers})
 }
 getcountvalider(){
  let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
  return this.http.get("http://127.0.0.1:8080/sous/getsousbyetatvalider",{headers:headers})
 }
 nonvalider(){
  let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
  return this.http.get("http://127.0.0.1:8080/sous/getsousbyetatnonvalider",{headers:headers})

 }

 getsouscriptionbydateandcat(date:any,id:number){let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
 return this.http.put("http://127.0.0.1:8080/sous/getsouscriptionbydateandcat/"+id,date,{headers:headers})}
 getsousbyaganddate(id:number,date:any)
 {let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
 return this.http.put("http://127.0.0.1:8080/sous/getsousbyaganddate/"+id,date,{headers:headers})}
 getsouscriptionbyageandcat(id1:any,id2:number){let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
 return this.http.get("http://127.0.0.1:8080/sous/getsousbyagandcat/"+id1+"/"+id2,{headers:headers})}
 getcountsouscriptionbyagence(id1:any){let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
 return this.http.get("http://127.0.0.1:8080/sous/getcountbyag/"+id1,{headers:headers})}
 getcountsouscriptionbyid(id1:any){let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
 return this.http.get("http://127.0.0.1:8080/sous/getsouscriptionbyid/"+id1,{headers:headers})}
 valider(id1:any,data){let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
 return this.http.put("http://127.0.0.1:8080/sous/valider/"+id1,data,{headers:headers})}
 souscriptionnonvaliderag(id:any){
  let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
   return this.http.get("http://127.0.0.1:8080/sous/souscriptionnonvaliderag/"+id,{headers:headers})
 }
 souscriptionvaliderag(id:any){
  let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
   return this.http.get("http://127.0.0.1:8080/sous/souscriptionvaliderag/"+id,{headers:headers})
 }
 souscriptiondateag(date1:any,date2:any,id:any){
  let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
   return this.http.get("http://127.0.0.1:8080/sous/souscriptionvaliderag/"+date1+"/"+date2+"/"+id,{headers:headers})
 }
 refusé(id:number,data){
  let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
   return this.http.put("http://127.0.0.1:8080/sous/refuser/"+id,data,{headers:headers})
 }
 nbresousva(){
  let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
  return this.http.get("http://127.0.0.1:8080/sous/nombrevalider/",{headers:headers})

 }
 nbresousnon(){
  let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
  return this.http.get("http://127.0.0.1:8080/sous/nbrnonvalider/",{headers:headers})

 }
 getsousbycatanddate(id:any,d1:any,d2:any){
  let headers=new HttpHeaders({"authorization":localStorage.getItem("token")})
  return this.http.get("http://127.0.0.1:8080/sous/getsousbycatanddate/"+id+"/"+d1+"/"+d2+"/",{headers:headers})
 }
 
}
