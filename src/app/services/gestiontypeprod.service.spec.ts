import { TestBed } from '@angular/core/testing';

import { GestiontypeprodService } from './gestiontypeprod.service';

describe('GestiontypeprodService', () => {
  let service: GestiontypeprodService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestiontypeprodService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
