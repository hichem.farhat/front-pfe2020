import { TestBed } from '@angular/core/testing';

import { GestioncaracterstiqueService } from './gestioncaracterstique.service';

describe('GestioncaracterstiqueService', () => {
  let service: GestioncaracterstiqueService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GestioncaracterstiqueService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
