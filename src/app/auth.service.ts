import { Injectable } from '@angular/core';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { GestionuserService } from './services/gestionuser.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( private route:Router) { }
canActivate(){
let token= localStorage.getItem("token")
if(token){
return true;
}else{
this.route.navigate(['/login']);
return false;
}
}
}