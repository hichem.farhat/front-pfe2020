import { Component, OnInit } from '@angular/core';
import { GestionuserService } from '../services/gestionuser.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { GestioncarteService } from '../services/gestioncarte.service';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Carte } from '../carte';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';


@Component({
  selector: 'app-carte',
  templateUrl: './carte.component.html',
  styleUrls: ['./carte.component.css']
})
export class CarteComponent implements OnInit {
  user:any=[]
  carte:any=[]
  ca:Carte=new Carte()
  @LocalStorage() token :any
  count:any
  cat:any=[]
  cars:any=[]
  searchtext:any
  p:number=1

  constructor(private serv5:GestioncategorieService,private serv4:GestioncaracterstiqueService,private serv:GestioncarteService,private serv2:GestionuserService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }

  ngOnInit(){
    this.getcount()
    this.getuser()
    this.getall()
    this.getallcat()
    this.getallcarac()
  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}

getallcat() {
    this.serv5.getallcat().subscribe(
        data => {
            this.cat = data;
            
        },
        err => {
            console.log(err);
        }
    );

}
  getuser(){
    this.serv2.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getall(){
    this.serv.getallcarte().subscribe(
      data=>{this.carte=data},
  erre=>{}

    )
  }
  add()
      {
      
      this.serv.addcarte(this.ca).subscribe(
  
        data=>{this.route.navigate(['/carte'])
      this.ngOnInit()},
        erre=>{}
      )
    
    
    
    }
  supprimer(id:number){
    if (window.confirm('Are sure you want to delete this Abonnee ?')){
    this.serv.deletecarte(id).subscribe(
      data => {
        console.log(data)
        this.toastr.success(' data successfully deleted!')
        this.ngOnInit() },
      err => { })
  
      }
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
    getcount(){
      this.serv.getlcount().subscribe(
        data=>{
          this.count=data;console.log('exected' +data);
        },
        err=>{
          console.log(err);
        }
      );
  
    }

}
