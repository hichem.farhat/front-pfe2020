import { Component, OnInit } from '@angular/core';
import { Client } from 'src/app/client';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { GestioncategorieService } from 'src/app/services/gestioncategorie.service';
import { GestionclientService } from 'src/app/services/gestionclient.service';
import { ToastrService } from 'ngx-toastr';
import { GestionagenceService } from 'src/app/services/gestionagence.service';
import { Router } from '@angular/router';
import { GestioncaracterstiqueService } from 'src/app/services/gestioncaracterstique.service';
import { GestionuserService } from 'src/app/services/gestionuser.service';

@Component({
  selector: 'app-addclt',
  templateUrl: './addclt.component.html',
  styleUrls: ['./addclt.component.css']
})
export class AddcltComponent implements OnInit {
  @LocalStorage() token:any;
  ag:any=[]
 cars:any=[]
  cat:any=[];
  client:Client= new Client();
  user:any
  constructor(private serv8: GestionuserService ,private serv4: GestioncaracterstiqueService ,private serv1: GestionclientService ,private serv2:GestioncategorieService,private serv3:GestionagenceService, private toastr: ToastrService,private route:Router ,private local:LocalStorageService) { }

  ngOnInit() {
   
    this.getallcatt();
    this.getallagences();
    this.getallcarac()
    this.getuser()
  }
  getuser(){
    this.serv8.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
  
    getallcatt(){

      this.serv2.getallcat().subscribe(
        data=>{this.cat=data},
    erre=>{}

      )
    }
    getallagences(){
      this.serv3.getallagence().subscribe(
        data=>{this.ag=data},
        erre=>{}
      )
    }

    
      add()
      {
      
      this.serv1.addclt(this.client).subscribe(
  
        data=>{this.route.navigate(['/client'])
      this.ngOnInit()},
        erre=>{}
      )
    
    
    
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }}
  


