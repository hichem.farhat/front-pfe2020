import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcltComponent } from './addclt.component';

describe('AddcltComponent', () => {
  let component: AddcltComponent;
  let fixture: ComponentFixture<AddcltComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcltComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcltComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
