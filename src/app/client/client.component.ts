import { Component, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { Client } from '../client';

import { Router } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestionclientService } from '../services/gestionclient.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestionagenceService } from '../services/gestionagence.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestionuserService } from '../services/gestionuser.service';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  @LocalStorage() token:any;
  ag:any=[]
  clt: any = [];
  cat:any=[];
  client:Client= new Client();
  cars: any = [];
  user:any
  searchtext:any
  p:number=1
  stat:any=[]
  count:any

  constructor(private serv8: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private serv1: GestionclientService ,private serv2:GestioncategorieService,private serv3:GestionagenceService, private toastr: ToastrService,private route:Router ,private local:LocalStorageService) { }

  ngOnInit() {
    this.getclt();
    this.getALLcAt()
    this.getallcarac()
    this.getuser()
    this.getpourcentage()
    this.getcount()
    
    
  }
  getuser(){
    this.serv8.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getcount(){this.serv1.countclt().subscribe(
    data => {
      this.count = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );



  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  getclt(){
    this.serv1.getallclt().subscribe(
      data => {
        this.clt = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
   
   
    supprimer(id:number){
      if (window.confirm('Are sure you want to delete this Abonnee ?')){
      this.serv1.delclt(id).subscribe(
        data => {
          console.log(data)
          this.toastr.success(' data successfully deleted!')
          this.ngOnInit() },
        err => { })
    
        }
      }
      add()
      {
      
      this.serv1.addclt(this.client).subscribe(
  
        data=>{this.route.navigate(['/client'])
      this.ngOnInit()},
        erre=>{}
      )
    
    
    
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
    getpourcentage() {
      this.serv1.getpourcentage().subscribe(
          data => {
              this.stat = data;
              //console.log('exected pr' + data);
              //console.log(data)
          },
          err => {
              console.log(err);
          }
      );
  }

  setMyStyles(id: any) {
      let styles = null;
      this.stat.forEach(function (value) {
          /* console.log("value");
          console.log(value);
          console.log(id);
          console.log(value.id == id); */
          if (value.id == id) {

              styles = {
                  'width': value.pr + '%',
              };
          }
      });
      return styles;

  }
 




  }
  