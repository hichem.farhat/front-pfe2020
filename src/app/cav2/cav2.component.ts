import { Component, OnInit } from '@angular/core';
import { GestionCompteAVueService } from '../services/gestion-compte-a-vue.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-cav2',
  templateUrl: './cav2.component.html',
  styleUrls: ['./cav2.component.css']
})
export class Cav2Component implements OnInit {
  cars:any=[]
  cav:any=[]
@LocalStorage() token:any
@LocalStorage() id:any
user:any
count:any
searchtext:any
p:number=1
  constructor(private carac:GestioncaracterstiqueService
,    private serv:GestionCompteAVueService,private serv2:GestionuserService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }

  ngOnInit() {
    this.getallcav()
    this.getuser()
    this.getcount()
    this.getallcarac()
  }
  getallcarac() {
    this.carac.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
  getuser(){
    this.serv2.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcav(){this.serv.getallCompteAVuebyagence(this.id).subscribe(
    data=>{this.cav=data},
erre=>{}

  )
}
supprimer(id:number){
  if (window.confirm('Are sure you want to delete this Abonnee ?')){
  this.serv.deleteCompteAVue(id).subscribe(
    data => {
      console.log(data)
      this.toastr.success(' data successfully deleted!')
      this.ngOnInit() },
    err => { })

    }
  }
  deconnexion() {
    this.local.clear("token")
    this.local.clear("id")
    this.route.navigate(['/login'])
}
  getcount(){
    this.serv.countag(this.id).subscribe(
      data=>{
        this.count=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
}