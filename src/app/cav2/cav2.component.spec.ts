import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Cav2Component } from './cav2.component';

describe('Cav2Component', () => {
  let component: Cav2Component;
  let fixture: ComponentFixture<Cav2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Cav2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Cav2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
