import { Categorie } from './categorie'
import { Agence } from './agence'

export class Client {
  id :number
	  nom :string
	  adresse:string
	 cin :string
	  date_creation :string
	  cat :Categorie
	  salaire:number
	  ag:Agence
	 
}
