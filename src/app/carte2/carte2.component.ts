import { Component, OnInit } from '@angular/core';
import { GestioncarteService } from '../services/gestioncarte.service';
import { GestionuserService } from '../services/gestionuser.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { Carte } from '../carte';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-carte2',
  templateUrl: './carte2.component.html',
  styleUrls: ['./carte2.component.css']
})
export class Carte2Component implements OnInit {

  user:any=[]
  carte:any=[]
  ca:Carte=new Carte()
  @LocalStorage() token :any
  @LocalStorage() id :any
  count:any
  searchtext:any
  p:number=1
  cars:any=[]


  constructor(private carac:GestioncaracterstiqueService
,    private serv:GestioncarteService,private serv2:GestionuserService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }

  ngOnInit(){
    this.getcount()
    this.getuser()
    this.getallcarac()
    this.getall()
  }
  getallcarac() {
    this.carac.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
  getuser(){
    this.serv2.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getall(){
    this.serv.getallcartebyagence(this.id).subscribe(
      data=>{this.carte=data},
  erre=>{}

    )
  }
  add()
      {
      
      this.serv.addcarte(this.ca).subscribe(
  
        data=>{this.route.navigate(['/carte'])
      this.ngOnInit()},
        erre=>{}
      )
    
    
    
    }
  supprimer(id:number){
    if (window.confirm('Are sure you want to delete this Abonnee ?')){
    this.serv.deletecarte(id).subscribe(
      data => {
        console.log(data)
        this.toastr.success(' data successfully deleted!')
        this.ngOnInit() },
      err => { })
  
      }
    }
    deconnexion() {
      this.local.clear("token")
      this.local.clear("id")
      this.route.navigate(['/login'])
  }
    getcount(){
      this.serv.countag(this.id).subscribe(
        data=>{
          this.count=data;console.log('exected' +data);
        },
        err=>{
          console.log(err);
        }
      );
  
    }

}
