import { Souscription } from './souscription'
import { Produit } from './produit'

export class ProdSous {
    id:number
    libelle :string
    sous:Souscription
    prod:Produit
    date:string

}
