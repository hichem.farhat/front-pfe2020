import { Component, OnInit } from '@angular/core';
import { GestionclientService } from '../services/gestionclient.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestionagenceService } from '../services/gestionagence.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestionuserService } from '../services/gestionuser.service';
import { Client } from '../client';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-cl2',
  templateUrl: './cl2.component.html',
  styleUrls: ['./cl2.component.css']
})
export class Cl2Component implements OnInit {
  @LocalStorage() token:any;
   @LocalStorage() id:any;
  
  ag:any=[]
  clt: any = [];
  cats:any=[];
  user=null
  client:Client= new Client();
  searchtext:any
  p:number=1
  cars:any=[]
  count:any
  stat:any
  cat:any=[]

  constructor(private carac: GestioncaracterstiqueService,private serv7: GestionuserService,private serv1: GestionclientService ,private serv2:GestioncategorieService,private serv3:GestionagenceService, private toastr: ToastrService,private route:Router ,private local:LocalStorageService) { }

  ngOnInit() {
    this.getuser()
    this.getclt()
    
    this.getallcatt();
    this.getallagences();
    this.getallcarac()
    this.getcount()
    this.getpourcentage()
    this.getALLcAt()
  }
  getALLcAt(){
    this.serv2.getallcat().subscribe(
      data => {
        this.cat = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
  getallcarac() {
    this.carac.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getcount(){
  this.serv1.getcountag(this.id).subscribe(
    data=>{
      this.count=data;
    },
    erre=>{console.log(erre)}
  )
}
  getuser(){
    this.serv7.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(this.user);
      }
    );

  }
  getclt()
  {
    this.serv1.getclientbyagence(this.id).subscribe(
      data=>{
        this.clt=data;
      },
      erre=>{console.log(erre)}
    )
  }
    getallcatt(){

      this.serv2.getallcat().subscribe(
        data=>{this.cats=data},
    erre=>{}

      )
    }
    getallagences(){
      this.serv3.getallagence().subscribe(
        data=>{this.ag=data},
        erre=>{}
      )
    }

    supprimer(id:number){
      if (window.confirm('Are sure you want to delete this Abonnee ?')){
      this.serv1.delclt(id).subscribe(
        data => {
          console.log(data)
          this.toastr.success(' data successfully deleted!')
          this.ngOnInit() },
        err => { })
    
        }
      }
      add()
      {
      
      this.serv1.addclt(this.client).subscribe(
  
        data=>{this.route.navigate(['/cl2'])
      this.ngOnInit()},
        erre=>{}
      )
    
    
    
    }
    deconnexion()
    {
      this.local.clear("token")
      this.local.clear("id")
      this.route.navigate(['/login'])
    }
    getpourcentage() {
      this.serv1.getpourcentage2(this.id).subscribe(
          data => {
              this.stat = data;
              //console.log('exected pr' + data);
              //console.log(data)
          },
          err => {
              console.log(err);
          }
      );
  }

  setMyStyles(id: any) {
      let styles = null;
      this.stat.forEach(function (value) {
          /* console.log("value");
          console.log(value);
          console.log(id);
          console.log(value.id == id); */
          if (value.id == id) {

              styles = {
                  'width': value.pr + '%',
              };
          }
      });
      return styles;

  }}