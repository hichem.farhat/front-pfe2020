import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Addcl2Component } from './addcl2.component';

describe('Addcl2Component', () => {
  let component: Addcl2Component;
  let fixture: ComponentFixture<Addcl2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Addcl2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Addcl2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
