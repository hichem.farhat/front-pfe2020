import { Component, OnInit } from '@angular/core';
import { GestionService } from '../gestion.service';
import { ProdSous } from '../prod-sous';
import { Router } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { ToastrService } from 'ngx-toastr';
import { ProduitService } from '../services/produit.service';
import { SouscriptionService } from '../services/souscription.service';
import { ProdsouscrireService } from '../services/prodsouscrire.service';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-prodsous',
  templateUrl: './prodsous.component.html',
  styleUrls: ['./prodsous.component.css']
})
export class ProdsousComponent implements OnInit {
sous:any=[]
prod:any=[]
prodss:any=[]
@LocalStorage() token:any;
cat:any=[]
cars:any=[]
user:any
searchtext:any
p:number=1

prodsous:ProdSous= new ProdSous();
  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private serv:ProdsouscrireService,private serv2:ProduitService,private serv3:SouscriptionService,private route:Router,private local:LocalStorageService,private toastr: ToastrService) { }

  ngOnInit() {
    this.getallsous();
    this.prods();
    this.getallprod();
    this.getuser()
    this.getallcarac()
    this.getALLcAt()
    this.prods()
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  getallsous(){
    this.serv3.getallsous().subscribe(
      data => {
        this.sous = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
    getallprod(){
      this.serv2.getallprod().subscribe(
        data => {
          this.prod = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
      }
      
      add(){
        {
      
          this.serv.saveprodsous(this.prodsous).subscribe(
      
            data=>{this.route.navigate(['/prodsous'])
          this.ngOnInit()},
            erre=>{}
          )
        
        
        
        }

      }
      delete(id:number){
        if (window.confirm('Are sure you want to delete this product ?')){
          this.serv.delprodsous(id).subscribe(
            data => {
              console.log(data)
              this.toastr.success(' data successfully deleted!')
              this.ngOnInit() },
            err => { })
        
            }
          }
        deconnexion()
        {
          this.local.clear("token")
          this.route.navigate(['/login'])
        }
        prods(){
          this.serv.getallprodsous().subscribe(
            data => {
              this.prodss = data; console.log('exected' + data);
            },
            err => {
              console.log(err);
            }
            );
          }
          



        

}
