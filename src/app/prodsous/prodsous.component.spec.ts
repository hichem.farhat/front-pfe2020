import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdsousComponent } from './prodsous.component';

describe('ProdsousComponent', () => {
  let component: ProdsousComponent;
  let fixture: ComponentFixture<ProdsousComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdsousComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdsousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
