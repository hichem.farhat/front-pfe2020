import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditsousComponent } from './editsous.component';

describe('EditsousComponent', () => {
  let component: EditsousComponent;
  let fixture: ComponentFixture<EditsousComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditsousComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditsousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
