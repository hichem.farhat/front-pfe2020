import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-navprod',
  templateUrl: './navprod.component.html',
  styleUrls: ['./navprod.component.css']
})
export class NavprodComponent implements OnInit {
  id:number
  constructor(private ac:ActivatedRoute,private route:Router) { 
    this.id=this.ac.snapshot.params['id'];
   }

  

  ngOnInit() {
    if(this.id!=null){
      this.route.navigate(["/prodfilter/"+this.id]);
    }

  }
}
