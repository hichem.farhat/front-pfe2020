import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { GestionService } from '../gestion.service';
import { Router } from '@angular/router';
import { Produit } from '../produit';
import { ToastrService } from 'ngx-toastr';
import { ProduitService } from '../services/produit.service';
import { GestiontypeprodService } from '../services/gestiontypeprod.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import * as jsPDF from 'jspdf'
@Component({
  selector: 'app-produits',
  templateUrl: './produits.component.html',
  styleUrls: ['./produits.component.css']
})
export class ProduitsComponent implements OnInit {
 
  prods: any = [];
  cars: any=[]
  user:any
  typeprods: any = [];
  produit:Produit= new Produit();
  cat: any=[]
  @LocalStorage()token :any
  searchtext:any
  p:number=1
  
  constructor(private serv5: GestioncategorieService,private serv6: GestionuserService,private local:LocalStorageService,private serv: ProduitService,private serv2:GestiontypeprodService,private serv3:GestioncaracterstiqueService, private route:Router,private toastr:ToastrService) { }

  ngOnInit() {
    this.getalltype();
    this.getallprods();
    this.getallcarac();
    this.getALLcAt()
    this.getuser()
   
  }
  deconnexion()
{
  this.local.clear("token")
  this.route.navigate(['/login'])
}
getuser(){
  this.serv6.getrole().subscribe(
    data=>{
      this.user=data;console.log('exected' +data);
    },
    err=>{
      console.log(err);
    }
  );

}

getALLcAt(){
this.serv5.getallcat().subscribe(
  data => {
    this.cat = data; console.log('exected' + data);
  },
  err => {
    console.log(err);
  }
  );
}
getallprods(){
  this.serv.getallprod().subscribe(    data => {
    this.prods = data; console.log('exected' + data);
  },
  err => {
    console.log(err);
  }
  );}
getalltype(){
  this.serv2.getalltypeprod().subscribe(
    data=>{
      this.typeprods=data;console.log('exected' +data);
    },
    err=>{
      console.log(err);
    }
  );



}
getallcarac(){
  this.serv3.getallcarac().subscribe(
    data=>{
      this.cars=data;console.log('exected' +data);
    },
    err=>{
      console.log(err);
    }
  );



}


add(){
  this.serv.addprod(this.produit).subscribe(
    data=>{this.route.navigate(['/prod'])
    this.ngOnInit()},
      erre=>{}
    )
  
  
  
  }
  delete(id:number){
    if (window.confirm('Are sure you want to delete this product ?')){
      this.serv.deleteprod(id).subscribe(
        data => {
          console.log(data)
          this.toastr.success(' data successfully deleted!')
          this.ngOnInit() },
        err => { })
    
        }
      }
     
     

  
}

