import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mdfpassword1Component } from './mdfpassword1.component';

describe('Mdfpassword1Component', () => {
  let component: Mdfpassword1Component;
  let fixture: ComponentFixture<Mdfpassword1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mdfpassword1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mdfpassword1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
