import { Component, OnInit } from '@angular/core';

import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { GestionuserService } from '../services/gestionuser.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  user1:any
 
  constructor(private gestion:GestionuserService,private local:LocalStorageService,private route:Router) { }

  ngOnInit() {
   this.getrole() 
   
  }
  getrole(){
    this.gestion.getrole().subscribe(
      data => {console.log(this.user1) 
        this.user1=data;console.log(this.user1.role);
        if (this.user1.role === "ADMIN") {
          this.route.navigate(['/ac'])
        }  if (this.user1.role === "USER") {
          this.local.store("id",this.user1.ag.id);


          this.route.navigate(['/ac2'])
          
        }
      },
      err => { console.log(err);})}
 


  }

