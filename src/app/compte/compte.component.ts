import { Component, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Compte } from '../compte';
import { GestionclientService } from '../services/gestionclient.service';
import { GestiontypecompteService } from '../services/gestiontypecompte.service';
import { GestioncompteService } from '../services/gestioncompte.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestionuserService } from '../services/gestionuser.service';

@Component({
  selector: 'app-compte',
  templateUrl: './compte.component.html',
  styleUrls: ['./compte.component.css']
})
export class CompteComponent implements OnInit {
  clt:any=[]
  ct: any = [];
  tcp: any = [];
  cars: any = [];
  cat: any = [];
  user: any 
  searchtext:any
  p:number=1

  comptes:Compte= new Compte();
  @LocalStorage() token:any;
  @LocalStorage() pass:any;

  constructor(private serv8: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private serv:GestioncompteService,private serv2:GestionclientService,private serv3:GestiontypecompteService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }

  ngOnInit() {
    this.getallct();
    this.getclt();
    this.gettypes();
    this.getALLcAt()
    this.getallcarac()
    this.getuser()
    console.log(this.pass)
    
    
  }  getuser(){
    this.serv8.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  getclt(){
    this.serv2.getallclt().subscribe(
      data => {
        this.clt = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
  getallct(){

    this.serv.getallcomptes().subscribe(
      data=>{this.ct=data},
  erre=>{}

    )
  }
    
      deconnexion()
    {
      this.local.clear("user")
      this.route.navigate(['/login'])
    }
    supprimer(id:number){
      if (window.confirm('Are sure you want to delete this Abonnee ?')){
      this.serv.deleteaccount(id).subscribe(
        data => {
          console.log(data)
          this.toastr.success(' data successfully deleted!')
          this.ngOnInit() },
        err => { })
    
        }
      }
      add()
      {
      
      this.serv.addaccount(this.comptes).subscribe(
  
        data=>{this.route.navigate(['/compte'])
      this.ngOnInit()},
        erre=>{}
      )
    
    
    
    }
    gettypes(){
      this.serv3.getalltypecompte().subscribe(
        data=>{this.tcp=data},
    erre=>{}
  
      )

    }

}
