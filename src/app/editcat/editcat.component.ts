import { Component, OnInit } from '@angular/core';

import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-editcat',
  templateUrl: './editcat.component.html',
  styleUrls: ['./editcat.component.css']
})
export class EditcatComponent implements OnInit {
  id:number;
  user:any
  @LocalStorage() token:any;
  cats=null
  cars:any=[]
  cat:any=[]
  message: string;
  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private ac:ActivatedRoute,private serv:GestioncategorieService,private toastr: ToastrService,private route:Router ,private local:LocalStorageService) { 
    this.id=this.ac.snapshot.params['id'];
  }

  ngOnInit() {
    this.gettcatbyid()
    this.getuser()
    this.getALLcAt()
    
    this.getallcarac()

  }  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  gettcatbyid(){this.serv.getcatbyid(this.id).subscribe(
    data => { this.cats=data
      console.log(data) },
    err => { console.log})}
    updatecat() {
      this.serv.updatecat(this.cats.id, this.cats)
        .subscribe(
          data=>{this.route.navigate(['/cat'])},
erre=>{}
);
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }

}
