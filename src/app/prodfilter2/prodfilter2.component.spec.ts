import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Prodfilter2Component } from './prodfilter2.component';

describe('Prodfilter2Component', () => {
  let component: Prodfilter2Component;
  let fixture: ComponentFixture<Prodfilter2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Prodfilter2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Prodfilter2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
