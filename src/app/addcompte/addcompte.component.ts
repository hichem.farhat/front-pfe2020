import { Component, OnInit } from '@angular/core';
import { Compte } from '../compte';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { GestioncompteService } from '../services/gestioncompte.service';
import { GestionclientService } from '../services/gestionclient.service';
import { GestiontypecompteService } from '../services/gestiontypecompte.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestionuserService } from '../services/gestionuser.service';

@Component({
  selector: 'app-addcompte',
  templateUrl: './addcompte.component.html',
  styleUrls: ['./addcompte.component.css']
})
export class AddcompteComponent implements OnInit {
  clt:any=[]
 
 cars:any=[]
 cat:any=[]
 user:any
  tcp: any = [];

  comptes:Compte= new Compte();
  @LocalStorage() token:any;

  constructor(private serv7:GestionuserService,private serv5:GestioncategorieService,private serv4:GestioncaracterstiqueService,private serv:GestioncompteService,private serv2:GestionclientService,private serv3:GestiontypecompteService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }

  ngOnInit() {this.getallcat()
    this.getuser()
    this.getallcat()
   
    this.getclt();
    this.gettypes();
    
    
  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}

getallcat() {
    this.serv5.getallcat().subscribe(
        data => {
            this.cat = data;
            
        },
        err => {
            console.log(err);
        }
    );

}

getuser() {
    this.serv7.getrole().subscribe(
        data => {
            this.user = data;
           // console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}

  getclt(){
    this.serv2.getallclt().subscribe(
      data => {
        this.clt = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
 
      deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
   
      add()
      {
      
      this.serv.addaccount(this.comptes).subscribe(
  
        data=>{this.route.navigate(['/compte'])
      this.ngOnInit()},
        erre=>{}
      )
    
    
    
    }
    gettypes(){
      this.serv3.getalltypecompte().subscribe(
        data=>{this.tcp=data},
    erre=>{}
  
      )

    }

}
