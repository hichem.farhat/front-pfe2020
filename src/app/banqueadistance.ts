import { Client } from './client'
import { Compte } from './compte'
import { ProdSous } from './prod-sous'

export class Banqueadistance {
    id:number
    prods:ProdSous
    cp:Compte
    num:number
    cl:Client
}
