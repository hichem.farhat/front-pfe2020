import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditcaracComponent } from './editcarac.component';

describe('EditcaracComponent', () => {
  let component: EditcaracComponent;
  let fixture: ComponentFixture<EditcaracComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditcaracComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditcaracComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
