import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';

@Component({
  selector: 'app-editcarac',
  templateUrl: './editcarac.component.html',
  styleUrls: ['./editcarac.component.css']
})
export class EditcaracComponent implements OnInit {
  ca=null
  message: string;
  id:number;
  cat:any=[]
cars:any=[]
@LocalStorage()token:any
user:any

  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private ac:ActivatedRoute,private serv:GestioncaracterstiqueService,private toastr: ToastrService,private route:Router ,private local:LocalStorageService) { 
    this.id=this.ac.snapshot.params['id'];
  }

  ngOnInit() {
    this.gettcaracbyid()
    this.getuser()
    this.getALLcAt()
    
    this.getallcarac()

  }  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  gettcaracbyid(){this.serv.getcaracbyid(this.id).subscribe(
    data => { this.ca=data
      console.log(data) },
    err => { console.log})}
    updateca() {
      this.serv.updatecarac(this.ca.id, this.ca)
        .subscribe(
          data=>{this.route.navigate(['/carac'])},
erre=>{}
);
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }


}
