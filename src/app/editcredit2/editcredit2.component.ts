import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GestioncreditService } from '../services/gestioncredit.service';
import { GestioncompteService } from '../services/gestioncompte.service';
import { GestionclientService } from '../services/gestionclient.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestionuserService } from '../services/gestionuser.service';

@Component({
  selector: 'app-editcredit2',
  templateUrl: './editcredit2.component.html',
  styleUrls: ['./editcredit2.component.css']
})
export class Editcredit2Component implements OnInit {
  clt:any=[]
  cp: any = [];
  cars: any = [];
  user:any
  
  @LocalStorage() token:any;
  @LocalStorage() id:any;
  credit=null
ids:number
  constructor(private serv6:GestionuserService,private carac:GestioncaracterstiqueService
,    private ac:ActivatedRoute,private serv:GestioncreditService,private serv2:GestioncompteService,private serv3:GestionclientService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { 
    this.ids=this.ac.snapshot.params['id'];}
  

  ngOnInit() {
    this.gettcredittbyid()
    this.getallcompte()
    this.getallclts()
    this.getallcarac()
    this.getuser()
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.carac.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
  getallcompte(){

    this.serv2.getallcomptebyagence(this.id).subscribe(
        data=>{this.cp=data},
    erre=>{}
  
      )
    }
    getallclts(){
      this.serv3.getclientbyagence(this.id).subscribe(
        data => {
          this.clt = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
      }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
    gettcredittbyid(){this.serv.getcreditbyid(this.ids).subscribe(
      data => { this.credit=data
        console.log(data) },
      err => { console.log})}
      updatecredit() {
        this.serv.update(this.credit.id, this.credit)
          .subscribe(
            data=>{this.route.navigate(['/credit2'])},
  erre=>{}
  );}

}
