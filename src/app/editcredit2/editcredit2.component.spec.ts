import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Editcredit2Component } from './editcredit2.component';

describe('Editcredit2Component', () => {
  let component: Editcredit2Component;
  let fixture: ComponentFixture<Editcredit2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Editcredit2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Editcredit2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
