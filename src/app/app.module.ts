import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgxWebstorageModule } from 'ngx-webstorage';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ClientComponent } from './client/client.component';
import { CategorieComponent } from './categorie/categorie.component';
import { element, promise } from 'protractor';
import { async } from '@angular/core/testing';
import { GoogleChartsModule } from 'angular-google-charts';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { SouscriptionComponent } from './souscription/souscription.component';
import { ProduitsComponent } from './produits/produits.component';
import { TypeproduitsComponent } from './typeproduits/typeproduits.component';
import { EdittypeComponent } from './edittype/edittype.component';
import { CompteComponent } from './compte/compte.component';
import { ProdsousComponent } from './prodsous/prodsous.component';
import { NavComponent } from './nav/nav.component';
import { AgenceComponent } from './agence/agence.component';
import { TypecompteComponent } from './typecompte/typecompte.component';
import { CreditComponent } from './credit/credit.component';
import { EditcltComponent } from './editclt/editclt.component';
import { EditcatComponent } from './editcat/editcat.component';
import { EditagComponent } from './editag/editag.component';
import { EdittcComponent } from './edittc/edittc.component';
import { EditaccountComponent } from './editaccount/editaccount.component';
import { EditsousComponent } from './editsous/editsous.component';
import { EditprodComponent } from './editprod/editprod.component';

import { ProdfilterComponent } from './prodfilter/prodfilter.component';
import { CltcatComponent } from './cltcat/cltcat.component';
import { CaracteristiqueComponent } from './caracteristique/caracteristique.component';
import { EditcaracComponent } from './editcarac/editcarac.component';
import { CreditnonComponent } from './creditnon/creditnon.component';
import { CreditouiComponent } from './creditoui/creditoui.component';
import { ChequeComponent } from './cheque/cheque.component';
import { CarteComponent } from './carte/carte.component';
import { RegisteruserComponent } from './registeruser/registeruser.component';
import { Cl2Component } from './cl2/cl2.component';
import { Editcl2Component } from './editcl2/editcl2.component';
import { Compte2Component } from './compte2/compte2.component';
import { Editcompte2Component } from './editcompte2/editcompte2.component';
import { UserComponent } from './user/user.component';
import { BanqueadistanceComponent } from './banqueadistance/banqueadistance.component';
import { CompteAVueComponent } from './compte-a-vue/compte-a-vue.component';
import { CompteepargneComponent } from './compteepargne/compteepargne.component';
import { Credit2Component } from './credit2/credit2.component';
import { Editcredit2Component } from './editcredit2/editcredit2.component';
import { EditcreditComponent } from './editcredit/editcredit.component';
import { Sous2Component } from './sous2/sous2.component';
import { Prodsous2Component } from './prodsous2/prodsous2.component';
import { Carte2Component } from './carte2/carte2.component';
import { Cheque2Component } from './cheque2/cheque2.component';
import { Cav2Component } from './cav2/cav2.component';
import { Bad2Component } from './bad2/bad2.component';
import { Ce2Component } from './ce2/ce2.component';
import { Ac2Component } from './ac2/ac2.component';
import { AddagComponent } from './agence/addag/addag.component';
import { AddcaracComponent } from './caracteristique/addcarac/addcarac.component';
import { AddcatComponent } from './categorie/addcat/addcat.component';
import { AddcltComponent } from './client/addclt/addclt.component';
import { AddcompteComponent } from './addcompte/addcompte.component';
import { EdituserComponent } from './user/edituser/edituser.component';
import { Addcl2Component } from './cl2/addcl2/addcl2.component';
import { AddsousComponent } from './souscription/addsous/addsous.component';
import { Addcredit2Component } from './credit2/addcredit2/addcredit2.component';
import { AddcreditComponent } from './credit/addcredit/addcredit.component';
import { Addcompte2Component } from './compte2/addcompte2/addcompte2.component';
import { Editsous2Component } from './editsous2/editsous2.component';
import { Addsous2Component } from './sous2/addsous2/addsous2.component';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {NgxPaginationModule} from 'ngx-pagination';
import { SousvaliderComponent } from './sousvalider/sousvalider.component';
import { SousnonvaliderComponent } from './sousnonvalider/sousnonvalider.component';
import { BetweenComponent } from './between/between.component';
import { AddtcpComponent } from './typecompte/addtcp/addtcp.component';
import { AddtpComponent } from './typeproduits/addtp/addtp.component';
import { Sousvalider2Component } from './sous2/sousvalider2/sousvalider2.component';
import { Sousnonvalider2Component } from './sous2/sousnonvalider2/sousnonvalider2.component';
import { Sousdate2Component } from './sous2/sousdate2/sousdate2.component';
import { Produits2Component } from './produits2/produits2.component';
import { Prodfilter2Component } from './prodfilter2/prodfilter2.component';
import { LoadingComponent } from './loading/loading.component';

import { PdfComponent } from './pdf/pdf.component';
import { Nav22Component } from './nav22/nav22.component';
import { NavprodComponent } from './navprod/navprod.component';
import { Navprod2Component } from './navprod2/navprod2.component';
import { MoncompteComponent } from './moncompte/moncompte.component';
import { Moncompte2Component } from './moncompte2/moncompte2.component';
import { StatComponent } from './stat/stat.component';
import { Mdfpassword1Component } from './mdfpassword1/mdfpassword1.component';
import { Mdfpassword2Component } from './mdfpassword2/mdfpassword2.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ClientComponent,
    CategorieComponent,
    
    
    AcceuilComponent,
    
    
    
    SouscriptionComponent,
    
    ProduitsComponent,
    
    TypeproduitsComponent,
    
    EdittypeComponent,
    
    CompteComponent,
    
    ProdsousComponent,
    
    NavComponent,
    
    AgenceComponent,
    
    TypecompteComponent,
    
    CreditComponent,
    
    EditcltComponent,
    
    EditcatComponent,
    
    EditagComponent,
    
    EdittcComponent,
    
    EditaccountComponent,
    
    EditsousComponent,
    
    EditprodComponent,
    

    
    ProdfilterComponent,
    
    CltcatComponent,
    
    CaracteristiqueComponent,
    
    EditcaracComponent,
    
    CreditnonComponent,
    
    CreditouiComponent,
    
    ChequeComponent,
    
    CarteComponent,
    
    RegisteruserComponent,
    
    Cl2Component,
    
    Editcl2Component,
    
    Compte2Component,
    
    Editcompte2Component,
    
    UserComponent,
    
    BanqueadistanceComponent,
    
    CompteAVueComponent,
    
    CompteepargneComponent,
    
    Credit2Component,
    
    Editcredit2Component,
    
    EditcreditComponent,
    
    Sous2Component,
    
    Prodsous2Component,
    
    Carte2Component,
    
    Cheque2Component,
    
    Cav2Component,
    
    Bad2Component,
    
    Ce2Component,
    
    Ac2Component,
    
    AddagComponent,
    
    AddcaracComponent,
    
    AddcatComponent,
    
    AddcltComponent,
    
    AddcompteComponent,
    
    EdituserComponent,
    
    Addcl2Component,
    
    AddsousComponent,
    
    Addcredit2Component,
    
    AddcreditComponent,
    
    Addcompte2Component,
    
    Editsous2Component,
    
    Addsous2Component,
    
    SousvaliderComponent,
    
    SousnonvaliderComponent,
    
    BetweenComponent,
    
    AddtcpComponent,
    
    AddtpComponent,
    
    Sousvalider2Component,
    
    Sousnonvalider2Component,
    
    Sousdate2Component,
    
    Produits2Component,
    
    Prodfilter2Component,
    
    LoadingComponent,
    
   
    
    PdfComponent,
    
   
    
    Nav22Component,
    
   
    
    NavprodComponent,
    
   
    
    Navprod2Component,
    
   
    
    MoncompteComponent,
    
   
    
    Moncompte2Component,
    
   
    
    StatComponent,
    
   
    
    Mdfpassword1Component,
    
   
    
    Mdfpassword2Component,
    
 
    
    
  ],
  imports: [
    
    BrowserModule,
    NgxPaginationModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    GoogleChartsModule,
    NgxWebstorageModule.forRoot(),
    MatSortModule,

    ToastrModule.forRoot(),
    Ng2SearchPipeModule,

    BrowserAnimationsModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
