import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompteAVueComponent } from './compte-a-vue.component';

describe('CompteAVueComponent', () => {
  let component: CompteAVueComponent;
  let fixture: ComponentFixture<CompteAVueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompteAVueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompteAVueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
