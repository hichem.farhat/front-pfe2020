import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-nav22',
  templateUrl: './nav22.component.html',
  styleUrls: ['./nav22.component.css']
})
export class Nav22Component implements OnInit {
id:number
  constructor(private ac:ActivatedRoute,private route:Router) { 
    this.id=this.ac.snapshot.params['id'];
   }

  

  ngOnInit() {
    if(this.id!=null){
      this.route.navigate(["/cltcat/"+this.id]);
    }

  }

}
