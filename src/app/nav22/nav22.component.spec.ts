import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Nav22Component } from './nav22.component';

describe('Nav22Component', () => {
  let component: Nav22Component;
  let fixture: ComponentFixture<Nav22Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Nav22Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Nav22Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
