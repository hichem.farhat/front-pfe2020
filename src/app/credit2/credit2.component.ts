import { Component, OnInit } from '@angular/core';
import { GestioncreditService } from '../services/gestioncredit.service';
import { GestioncompteService } from '../services/gestioncompte.service';
import { GestionclientService } from '../services/gestionclient.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Credit } from '../credit';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-credit2',
  templateUrl: './credit2.component.html',
  styleUrls: ['./credit2.component.css']
})
export class Credit2Component implements OnInit {
  clt:any=[]
  cp: any = [];
  cr:any=[]
  @LocalStorage() token:any;
  @LocalStorage() id:any;
  creditt:any
  ids:any
  user:any
  searchtext:any
  p:number=1
  cars:any=[]
  count:any
  pourcentage1:any
  pourcentage2:any
 
  credit:Credit= new Credit();
  
  crtt:Credit= new Credit();

  constructor(private carac:GestioncaracterstiqueService,private serv6:GestionuserService,private ac:ActivatedRoute,private serv:GestioncreditService,private serv2:GestioncompteService,private serv3:GestionclientService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }

  ngOnInit() {
    this.getp1()
    this.getp2()
    this.getcount()
    this.getallcompte()
    this.getallclts()
    this.getallcredit()
    this.getuser()
    this.getallcarac()

  }getallcarac() {
    this.carac.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getp1(){
  this.serv.getpourcent11(this.id).subscribe(
      data => {
          this.pourcentage1 = data;
         // console.log('exected' + data);
      },
      err => {
          console.log(err);
      }
  );
}
getp2(){this.serv.getpourcent22(this.id).subscribe(
  data => {
      this.pourcentage2 = data;
     // console.log('exected' + data);
  },
  err => {
      console.log(err);
  }
);} 
setMyStyles() {
let styles = null;

    /* console.log("value");
    console.log(value);
    console.log(id);
    console.log(value.id == id); */
   

        styles = {
            'width': this.pourcentage1 + '%',
             
           
        };
    

return styles;

}
setMyStyles2() {
let styles = null;

  /* console.log("value");
  console.log(value);
  console.log(id);
  console.log(value.id == id); */
 

      styles = {
          'width': this.pourcentage2 + '%',
         
      };
  

return styles;

}
getcount(){
  this.serv.getcountag(this.id).subscribe(
    data=>{
      this.count=data;console.log('exected' +data);
    },
    err=>{
      console.log(err);
    }
  );
}
  
  
  
  
  
  add(){

    this.serv.savecredit(this.credit).subscribe(
  
      data=>{
        this.route.navigate(['/credit2'])
    this.ngOnInit()},
      erre=>{}
    )
  
  
  
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcompte(){

  this.serv2.getallcomptebyagence(this.id).subscribe(
      data=>{this.cp=data},
  erre=>{}

    )
  }
  getallclts(){
    this.serv3.getclientbyagence(this.id).subscribe(
      data => {
        this.clt = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
  deconnexion()
  {
    this.local.clear("token")
    this.local.clear("id")
    this.route.navigate(['/login'])
  }
  delete(id){
    if (window.confirm('Are sure you want to delete this credit ?')){
      this.serv.delcredit(id).subscribe(
        data => {
          console.log(data)
          this.toastr.success(' data successfully deleted!')
          this.ngOnInit() },
        err => { })
    
        }
      }
      getallcredit(){

        this.serv.getcreditbyagence(this.id).subscribe(
          data=>{this.cr=data},
      erre=>{}
    
        )
      }
      getcreditbyid(ids){this.serv.getcreditbyid(this.id).subscribe(
        data => { this.creditt=data
          console.log(data) },
        err => { console.log})}
       
        validation(ids) {
        
          this.serv.validation(ids,this.crtt )
            .subscribe(
              data=>{this.ngOnInit()},
    erre=>{}
    );
        }
}


