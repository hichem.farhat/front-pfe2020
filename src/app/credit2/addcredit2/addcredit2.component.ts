import { Component, OnInit } from '@angular/core';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { ActivatedRoute, Router } from '@angular/router';
import { GestioncreditService } from 'src/app/services/gestioncredit.service';
import { GestionclientService } from 'src/app/services/gestionclient.service';
import { ToastrService } from 'ngx-toastr';
import { GestioncompteService } from 'src/app/services/gestioncompte.service';
import { Credit } from 'src/app/credit';
import { GestionuserService } from 'src/app/services/gestionuser.service';
import { GestioncaracterstiqueService } from 'src/app/services/gestioncaracterstique.service';

@Component({
  selector: 'app-addcredit2',
  templateUrl: './addcredit2.component.html',
  styleUrls: ['./addcredit2.component.css']
})
export class Addcredit2Component implements OnInit {
  clt:any=[]
  cp: any = [];
  cr:any=[]
  @LocalStorage() token:any;
  @LocalStorage() id:any;
  creditt:any
  ids:any
  user:any
  cars:any=[]
 
 
  credit:Credit= new Credit();
  
 

  constructor(private carac:GestioncaracterstiqueService
,    private serv6:GestionuserService,private ac:ActivatedRoute,private serv:GestioncreditService,private serv2:GestioncompteService,private serv3:GestionclientService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }

  ngOnInit() {
    this.getallcompte()
    this.getallclts()
    this.getuser()
    this.getallcarac()
   

  }
  getallcarac() {
    this.carac.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}add(){

    this.serv.savecredit(this.credit).subscribe(
  
      data=>{
        this.route.navigate(['/credit2'])
    this.ngOnInit()},
      erre=>{}
    )
  
  
  
  }
  getallcompte(){

  this.serv2.getallcomptebyagence(this.id).subscribe(
      data=>{this.cp=data},
  erre=>{}

    )
  }
  getallclts(){
    this.serv3.getclientbyagence(this.id).subscribe(
      data => {
        this.clt = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
    deconnexion() {
      this.local.clear("token")
      this.local.clear("id")
      this.route.navigate(['/login'])
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  
     
    
       
       
}
