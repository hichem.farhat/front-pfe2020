import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Addcredit2Component } from './addcredit2.component';

describe('Addcredit2Component', () => {
  let component: Addcredit2Component;
  let fixture: ComponentFixture<Addcredit2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Addcredit2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Addcredit2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
