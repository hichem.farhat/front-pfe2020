import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Credit2Component } from './credit2.component';

describe('Credit2Component', () => {
  let component: Credit2Component;
  let fixture: ComponentFixture<Credit2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Credit2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Credit2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
