import { Component, OnInit } from '@angular/core';
import { GestionService } from '../gestion.service';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Credit } from '../credit';
import { GestioncreditService } from '../services/gestioncredit.service';
import { GestioncompteService } from '../services/gestioncompte.service';
import { GestionclientService } from '../services/gestionclient.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestionuserService } from '../services/gestionuser.service';
import { ProduitService } from '../services/produit.service';

import * as jsPDF from 'jspdf';
import 'jspdf-autotable';


@Component({
  selector: 'app-credit',
  templateUrl: './credit.component.html',
  styleUrls: ['./credit.component.css']
})
export class CreditComponent implements OnInit {
  head = [['ID', 'client', 'montant', 'compte','date','salaire']]
 
  clt:any=[]
  cp: any = [];
  cr:any=[]
  @LocalStorage() token:any;
  creditt:any
  id:number
  cat:any=[]
  cars:any=[]
 user:any
  credit:Credit= new Credit();
  searchtext:any
  p:number=1
  p1:any
  p2:any
  prod:any=[]
  count:any
  data:any=[]
  
   
  
  crtt:Credit= new Credit();
  constructor(private prodd:ProduitService,private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private ac:ActivatedRoute,private serv:GestioncreditService,private serv2:GestioncompteService,private serv3:GestionclientService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { 
    
  }


  ngOnInit() {
    this.getallcompte()
    this.getallclts()
    this.getallcredit()
    this.getALLcAt()
      this.getallcarac()
      this.getuser()
      this.getp1()
      this.getp2()
      this.getallprod()
      this.getcount()
    

  }
  getcount(){
    this.serv.getcount().subscribe(
      data=>{
        this.count=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallprod() {
    this.prodd.getallprod().subscribe(
        data => {
            this.prod = data;
            //console.log('exected' + data);
        },
        err => {
            //console.log(err);
        }
    );

}
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  
  
  
  add(){

    this.serv.savecredit(this.credit).subscribe(
  
      data=>{this.route.navigate(['/credit'])
    this.ngOnInit()},
      erre=>{}
    )
  
  
  
  }


  getallcompte(){

  this.serv2.getallcomptes().subscribe(
      data=>{this.cp=data},
  erre=>{}

    )
  }
  getallclts(){
    this.serv3.getallclt().subscribe(
      data => {
        this.clt = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
  deconnexion()
  {
    this.local.clear("token")
    this.route.navigate(['/login'])
  }
  delete(id){
    if (window.confirm('Are sure you want to delete this credit ?')){
      this.serv.delcredit(id).subscribe(
        data => {
          console.log(data)
          this.toastr.success(' data successfully deleted!')
          this.ngOnInit() },
        err => { })
    
        }
      }
      getallcredit(){

        this.serv.getallcredit().subscribe(
          data=>{this.cr=data},
      erre=>{}
    
        )
      }
      getcreditbyid(id){this.serv.getcreditbyid(this.id).subscribe(
        data => { this.creditt=data
          console.log(data) },
        err => { console.log})}
       
        validation(id) {
        
          this.serv.validation(id,this.crtt )
            .subscribe(
              data=>{this.ngOnInit()},
    erre=>{}
    );
        }
        getp1(){
          this.serv.getpourcent1().subscribe(
              data => {
                  this.p1 = data;
                 // console.log('exected' + data);
              },
              err => {
                  console.log(err);
              }
          );
      }
      getp2(){this.serv.getpourcent2().subscribe(
          data => {
              this.p2 = data;
             // console.log('exected' + data);
          },
          err => {
              console.log(err);
          }
      );} 
      setMyStyles() {
        let styles = null;
        
            /* console.log("value");
            console.log(value);
            console.log(id);
            console.log(value.id == id); */
           

                styles = {
                    'width': this.p1 + '%',
                     
                   
                };
            
        
        return styles;

    }
    setMyStyles2() {
      let styles = null;
      
          /* console.log("value");
          console.log(value);
          console.log(id);
          console.log(value.id == id); */
         

              styles = {
                  'width': this.p2 + '%',
                 
              };
          
      
      return styles;

  }
 
}
