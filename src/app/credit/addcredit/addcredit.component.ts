import { Component, OnInit } from '@angular/core';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { Credit } from 'src/app/credit';
import { GestionuserService } from 'src/app/services/gestionuser.service';
import { GestioncategorieService } from 'src/app/services/gestioncategorie.service';
import { GestioncreditService } from 'src/app/services/gestioncredit.service';
import { GestioncaracterstiqueService } from 'src/app/services/gestioncaracterstique.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GestioncompteService } from 'src/app/services/gestioncompte.service';
import { GestionclientService } from 'src/app/services/gestionclient.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-addcredit',
  templateUrl: './addcredit.component.html',
  styleUrls: ['./addcredit.component.css']
})
export class AddcreditComponent implements OnInit {

  clt:any=[]
  cp: any = [];
  cr:any=[]
  @LocalStorage() token:any;
  creditt:any
  id:number
  cat:any=[]
  cars:any=[]
 user:any
  credit:Credit= new Credit();
  
 
  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private ac:ActivatedRoute,private serv:GestioncreditService,private serv2:GestioncompteService,private serv3:GestionclientService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { 
    
  }


  ngOnInit() {
    this.getallcompte()
    this.getallclts()
    
    this.getALLcAt()
      this.getallcarac()
      this.getuser()
    

  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  
  
  
  add(){

    this.serv.savecredit(this.credit).subscribe(
  
      data=>{this.route.navigate(['/credit'])
    this.ngOnInit()},
      erre=>{}
    )
  
  
  
  }


  getallcompte(){

  this.serv2.getallcomptes().subscribe(
      data=>{this.cp=data},
  erre=>{}

    )
  }
  getallclts(){
    this.serv3.getallclt().subscribe(
      data => {
        this.clt = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
  deconnexion()
  {
    this.local.clear("token")
    this.route.navigate(['/login'])
  }
 
     
    
       
      }