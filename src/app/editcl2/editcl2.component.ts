import { Component, OnInit } from '@angular/core';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestionclientService } from '../services/gestionclient.service';
import { GestionagenceService } from '../services/gestionagence.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestionuserService } from '../services/gestionuser.service';

@Component({
  selector: 'app-editcl2',
  templateUrl: './editcl2.component.html',
  styleUrls: ['./editcl2.component.css']
})
export class Editcl2Component implements OnInit {
  id:number;
  client=null
  message: string;
  @LocalStorage() token:any;
  @LocalStorage() ids:any;
  cats:any=[];
  ag:any=[]
  cars:any=[]
  user:any

  constructor(private serv6:GestionuserService,private carac:GestioncaracterstiqueService,private serv3:GestioncategorieService, private ac:ActivatedRoute,private serv : GestionclientService,private serv2:GestionagenceService, private route:Router, private local:LocalStorageService) { 
    this.id=this.ac.snapshot.params['id'];
  }

  ngOnInit() {
    this.gettcltbyid()
    this.getallcatt()
    this.getallagences()
    this.getuser()
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.carac.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}

  gettcltbyid(){this.serv.getcltbyid(this.id).subscribe(
    data => { this.client=data
      console.log(data) },
    err => { console.log})}
    updateclt() {
      this.serv.updateclt(this.client.id, this.client)
        .subscribe(
          data=>{this.route.navigate(['/cl2'])},
erre=>{}
);
    }
    deconnexion()
    {
      this.local.clear("token")
      this.local.clear("ids")
      this.route.navigate(['/login'])
    }
    getallcatt(){

      this.serv3.getallcat().subscribe(
        data=>{this.cats=data},
    erre=>{}

      )
    }
    getallagences(){
      this.serv2.getallagence().subscribe(
        data=>{this.ag=data},
        erre=>{}
      )
    }


}

