import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Editcl2Component } from './editcl2.component';

describe('Editcl2Component', () => {
  let component: Editcl2Component;
  let fixture: ComponentFixture<Editcl2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Editcl2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Editcl2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
