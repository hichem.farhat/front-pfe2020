import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SousnonvaliderComponent } from './sousnonvalider.component';

describe('SousnonvaliderComponent', () => {
  let component: SousnonvaliderComponent;
  let fixture: ComponentFixture<SousnonvaliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SousnonvaliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SousnonvaliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
