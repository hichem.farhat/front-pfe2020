import { Component, OnInit } from '@angular/core';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { SouscriptionService } from '../services/souscription.service';
import { GestionuserService } from '../services/gestionuser.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-sousnonvalider',
  templateUrl: './sousnonvalider.component.html',
  styleUrls: ['./sousnonvalider.component.css']
})
export class SousnonvaliderComponent implements OnInit {

  @LocalStorage () token:any
  user:any
  cars:any=[]
  cat:any=[]
  sous:any=[]
  constructor(private serv5: GestioncategorieService ,private serv8: GestioncaracterstiqueService ,private serv:SouscriptionService,private serv6: GestionuserService,private local:LocalStorageService,private route:Router,private toastr: ToastrService) { }

  ngOnInit() {
    this.getuser()
    this.getallsous()
    this.getallcarac()
    this.getALLcAt()

  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallsous(){
    this.serv.nonvalider().subscribe(
      data => {
        this.sous = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
    getallcarac() {
      this.serv8.getallcarac().subscribe(
          data => {
              this.cars = data;
            //  console.log('exected' + data);
          },
          err => {
              console.log(err);
          }
      );
  
  }
  
  getALLcAt(){
    this.serv5.getallcat().subscribe(
      data => {
        this.cat = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }

}
