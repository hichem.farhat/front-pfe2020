import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdittcComponent } from './edittc.component';

describe('EdittcComponent', () => {
  let component: EdittcComponent;
  let fixture: ComponentFixture<EdittcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdittcComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdittcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
