import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';

import { GestiontypecompteService } from '../services/gestiontypecompte.service';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';

@Component({
  selector: 'app-edittc',
  templateUrl: './edittc.component.html',
  styleUrls: ['./edittc.component.css']
})
export class EdittcComponent implements OnInit {
  id:number;
  tcp=null
  message: string;
  @LocalStorage() token:any;
  user:any
  cat:any=[]
  cars:any=[]
  
  

  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private ac:ActivatedRoute,private serv : GestiontypecompteService,private route:Router, private local:LocalStorageService) {
    this.id=this.ac.snapshot.params['id'];
   }

  ngOnInit(){
    this.gettcpbyid()
    this.getALLcAt()
    this.getallcarac()
    this.getuser()
    
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  gettcpbyid(){this.serv.gettypecompte(this.id).subscribe(
    data => { this.tcp=data
      console.log(data) },
    err => { console.log})}
    updatetcp() {
      this.serv.updatetcp(this.tcp.id, this.tcp)
        .subscribe(
          data=>{this.route.navigate(['/tcpp'])},
erre=>{}
);
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }

}
