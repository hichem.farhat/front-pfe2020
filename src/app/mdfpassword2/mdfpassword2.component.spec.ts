import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Mdfpassword2Component } from './mdfpassword2.component';

describe('Mdfpassword2Component', () => {
  let component: Mdfpassword2Component;
  let fixture: ComponentFixture<Mdfpassword2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mdfpassword2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mdfpassword2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
