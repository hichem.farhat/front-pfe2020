import { Component, OnInit } from '@angular/core';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestionagenceService } from '../services/gestionagence.service';
import { GestionuserService } from '../services/gestionuser.service';
import { Router } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';

@Component({
  selector: 'app-mdfpassword2',
  templateUrl: './mdfpassword2.component.html',
  styleUrls: ['./mdfpassword2.component.css']
})
export class Mdfpassword2Component implements OnInit {
  @LocalStorage() token:any;
  cars:any=[]
  ag:any=[]
  cat:any=[]
  user:any
  aa:any
  users:any
  s:any

  constructor(private serv5:GestioncategorieService,private serv4:GestioncaracterstiqueService , private serv2 : GestionagenceService,private serv : GestionuserService,private router:Router,private local:LocalStorageService) { 
    
  }

  ngOnInit() {
    this.getALLcAt()
    this.getallagences()
    this.getallcarac()
    this.getuser()
   
  }
  getuser(){
    this.serv.getrole().subscribe(
      data=>{
        this.user=data;
        console.log(data);
      },
      err=>{
        console.log(err);
      }
    );}

    updatuser() {
      console.log(this.user)
      
      
      //this.user.password=this.aa
      console.log(this.user)
      
      this.serv.updateass(this.user)
        .subscribe(
          data=>{
            this.deconnexion()
          
          
          
          
          
            
             
            }, err => {
              console.log(err);
          }
      );
            

    } 
   
  deconnexion()
  {
    this.local.clear("token")
    this.router.navigate(['/login'])
  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  
     
     
      getallagences(){this.serv2.getallagence().subscribe(
        data => {
          this.ag = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
  
  
  
    }

}
