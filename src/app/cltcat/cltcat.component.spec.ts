import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CltcatComponent } from './cltcat.component';

describe('CltcatComponent', () => {
  let component: CltcatComponent;
  let fixture: ComponentFixture<CltcatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CltcatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CltcatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
