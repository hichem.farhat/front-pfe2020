import { Component, OnInit } from '@angular/core';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { GestionclientService } from '../services/gestionclient.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestionuserService } from '../services/gestionuser.service';

@Component({
  selector: 'app-cltcat',
  templateUrl: './cltcat.component.html',
  styleUrls: ['./cltcat.component.css']
})
export class CltcatComponent implements OnInit {
 clt:any=[]
 cat:any=[]
 cars:any=[]
  id:any
  user:any
  searchtext:any
  p:number=1
  @LocalStorage() token:any;
  constructor(private serv8: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private ac:ActivatedRoute,private local:LocalStorageService,private serv: GestionclientService,private route:Router,private toastr:ToastrService) {
    this.id=this.ac.snapshot.params['id'];
   }

  ngOnInit() {
    this.getallcltbycat()
    this.getuser()
    this.getallcarac()
    this.getALLcAt()
   
 
  }
  getuser(){
    this.serv8.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcltbycat(){
    this.serv.getallclientbycat(this.id).subscribe(    data => {
      this.clt = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );}
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
    getallcarac() {
      this.serv4.getallcarac().subscribe(
          data => {
              this.cars = data;
            //  console.log('exected' + data);
          },
          err => {
              console.log(err);
          }
      );
  
  }
  getALLcAt(){
    this.serv5.getallcat().subscribe(
      data => {
        this.cat = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
}
