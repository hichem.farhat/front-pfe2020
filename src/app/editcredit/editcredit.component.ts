import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GestioncompteService } from '../services/gestioncompte.service';
import { GestioncreditService } from '../services/gestioncredit.service';
import { GestionclientService } from '../services/gestionclient.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-editcredit',
  templateUrl: './editcredit.component.html',
  styleUrls: ['./editcredit.component.css']
})
export class EditcreditComponent implements OnInit {
  clt:any=[]
  cp: any = [];
  cat: any = [];
  cars: any = [];
  
  @LocalStorage() token:any;
  credit=null
  id:number
  user:any
  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private ac:ActivatedRoute,private serv:GestioncreditService,private serv2:GestioncompteService,private serv3:GestionclientService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { 
    this.id=this.ac.snapshot.params['id'];
  }

  ngOnInit() {
    this.getallcarac()
    this.getuser()
    this.getALLcAt()
    this.gettcredittbyid()
    this.getallcompte()
    this.getallclts()
  }  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  gettcredittbyid(){this.serv.getcreditbyid(this.id).subscribe(
    data => { this.credit=data
      console.log(data) },
    err => { console.log})}
    updatecredit() {
      this.serv.update(this.credit.id, this.credit)
        .subscribe(
          data=>{this.route.navigate(['/credit'])},
erre=>{}
);}
  getallcompte(){

    this.serv2.getallcomptes().subscribe(
        data=>{this.cp=data},
    erre=>{}
  
      )
    }
    getallclts(){
      this.serv3.getallclt().subscribe(
        data => {
          this.clt = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
      }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }

}
