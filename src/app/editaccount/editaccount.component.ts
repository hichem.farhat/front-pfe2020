import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestiontypecompteService } from '../services/gestiontypecompte.service';
import { GestionclientService } from '../services/gestionclient.service';
import { GestioncompteService } from '../services/gestioncompte.service';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-editaccount',
  templateUrl: './editaccount.component.html',
  styleUrls: ['./editaccount.component.css']
})
export class EditaccountComponent implements OnInit {
  @LocalStorage() token:any;
  id:number;
  tcp: any = [];
  clt: any = [];
  comptes=null
  message: string;
  cat:any=[]
cars:any=[]
user:any
  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private ac:ActivatedRoute,private serv1:GestioncompteService,private serv2:GestiontypecompteService,private serv3:GestionclientService,private toastr: ToastrService,private route:Router ,private local:LocalStorageService) { 
    this.id=this.ac.snapshot.params['id'];
  }

  ngOnInit() {
    this.gettcomptebyid()
    this.getclt()
    this.gettypes()
    this.getallcarac()
    this.getuser()
    this.getALLcAt()

  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  gettcomptebyid(){this.serv1.getaccountbyid(this.id).subscribe(
    data => { this.comptes=data
      console.log(data) },
    err => { console.log})}
    updatecp() {
      this.serv1.updatecompte(this.comptes.id, this.comptes)
        .subscribe(
          data=>{this.route.navigate(['/compte'])},
erre=>{}
);
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
    getclt(){
      this.serv3.getallclt().subscribe(
        data => {
          this.clt = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
      }
      gettypes(){
        this.serv2.getalltypecompte().subscribe(
          data=>{this.tcp=data},
      erre=>{}
    
        )
  
      }

}
