import { Component, OnInit } from '@angular/core';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { SouscriptionService } from '../services/souscription.service';
import { GestionuserService } from '../services/gestionuser.service';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-between',
  templateUrl: './between.component.html',
  styleUrls: ['./between.component.css']
})
export class BetweenComponent implements OnInit {
debut:any
fin :any
  @LocalStorage () token:any
  user:any
  cars:any=[]
  cat:any=[]
  sous:any=[]
  constructor(private serv5: GestioncategorieService ,private serv8: GestioncaracterstiqueService ,private serv:SouscriptionService,private serv6: GestionuserService,private local:LocalStorageService,private route:Router,private toastr: ToastrService) { }

  ngOnInit() {
    this.getuser()
    this.getallsous()
    this.getallcarac()
    this.getALLcAt()
    this.getallsous()

  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallsous(){
   
    this.serv.getsouscriptionbydate(this.debut,this.fin).subscribe(
      
      data => {
        console.log("debut"+this.debut)
        this.sous = data; 
        console.log('cccc' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
    getallcarac() {
      this.serv8.getallcarac().subscribe(
          data => {
              this.cars = data;
            //  console.log('exected' + data);
          },
          err => {
              console.log(err);
          }
      );
  
  }
  
  getALLcAt(){
    this.serv5.getallcat().subscribe(
      data => {
        this.cat = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }

}
