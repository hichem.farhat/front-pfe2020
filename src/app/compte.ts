import { Client } from './client'
import { TypeCompte } from './type-compte'


export class Compte {
    id:number
    code:string
    cl:Client 
    libelle:string
    date:string
    cp:TypeCompte

}
