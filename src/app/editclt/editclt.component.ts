import { Component, OnInit } from '@angular/core';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { ActivatedRoute, Router } from '@angular/router';
import { GestionService } from '../gestion.service';
import { GestionclientService } from '../services/gestionclient.service';
import { GestionagenceService } from '../services/gestionagence.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-editclt',
  templateUrl: './editclt.component.html',
  styleUrls: ['./editclt.component.css']
})
export class EditcltComponent implements OnInit {
  id:number;
  client=null
  message: string;
  @LocalStorage() user:any;
  cat:any=[];
  cars:any=[];
  ag:any=[]
  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private serv3:GestioncategorieService, private ac:ActivatedRoute,private serv : GestionclientService,private serv2:GestionagenceService, private route:Router, private local:LocalStorageService) { 
    this.id=this.ac.snapshot.params['id'];
  }

  ngOnInit() {
    this.getuser()
    this.getallcarac()
    
    this.gettcltbyid()
    this.getallcatt()
    this.getallagences()
  }  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}

  gettcltbyid(){this.serv.getcltbyid(this.id).subscribe(
    data => { this.client=data
      console.log(data) },
    err => { console.log})}
    updateclt() {
      this.serv.updateclt(this.client.id, this.client)
        .subscribe(
          data=>{this.route.navigate(['/client'])},
erre=>{}
);
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
    getallcatt(){

      this.serv3.getallcat().subscribe(
        data=>{this.cat=data},
    erre=>{}

      )
    }
    getallagences(){
      this.serv2.getallagence().subscribe(
        data=>{this.ag=data},
        erre=>{}
      )
    }


}
