import { Component, OnInit } from '@angular/core';
import { ProdSous } from '../prod-sous';
import { ProdsouscrireService } from '../services/prodsouscrire.service';
import { ProduitService } from '../services/produit.service';
import { SouscriptionService } from '../services/souscription.service';
import { Router } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { ToastrService } from 'ngx-toastr';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestionuserService } from '../services/gestionuser.service';

@Component({
  selector: 'app-prodsous2',
  templateUrl: './prodsous2.component.html',
  styleUrls: ['./prodsous2.component.css']
})
export class Prodsous2Component implements OnInit {
  sous:any=[]
  prod:any=[]
  prodss:any=[]
  @LocalStorage() token:any;
  @LocalStorage() id:any;
  searchtext:any
  p:number=1
  cars:any=[]
  user:any
  
  prodsous:ProdSous= new ProdSous();
    constructor(private serv6:GestionuserService,private carac:GestioncaracterstiqueService,private serv:ProdsouscrireService,private serv2:ProduitService,private serv3:SouscriptionService,private route:Router,private local:LocalStorageService,private toastr: ToastrService) { }
  
    ngOnInit() {
      this.getallsous();
      this.prods();
      this.getallprod();
      this.getallcarac()
      this.getuser()
    }
    getallcarac() {
      this.carac.getallcarac().subscribe(
          data => {
              this.cars = data;
            //  console.log('exected' + data);
          },
          err => {
              console.log(err);
          }
      );
  
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
    getallsous(){
      this.serv3.getallsous().subscribe(
        data => {
          this.sous = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
      }
      getallprod(){
        this.serv2.getallprod().subscribe(
          data => {
            this.prod = data; console.log('exected' + data);
          },
          err => {
            console.log(err);
          }
          );
        }
        
        add(){
          {
        
            this.serv.saveprodsous(this.prodsous).subscribe(
        
              data=>{this.route.navigate(['/prodsous2'])
            this.ngOnInit()},
              erre=>{}
            )
          
          
          
          }
  
        }
        delete(id:number){
          if (window.confirm('Are sure you want to delete this product ?')){
            this.serv.delprodsous(id).subscribe(
              data => {
                console.log(data)
                this.toastr.success(' data successfully deleted!')
                this.ngOnInit() },
              err => { })
          
              }
            }
          deconnexion()
          {
            this.local.clear("token")
            this.route.navigate(['/login'])
          }
          prods(){
            this.serv.getallprodsousbyag(this.id).subscribe(
              data => {
                this.prodss = data; console.log('exected' + data);
              },
              err => {
                console.log(err);
              }
              );
            }
            
  
  
  
          
  
  }
  