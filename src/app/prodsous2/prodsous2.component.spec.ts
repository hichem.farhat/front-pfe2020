import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Prodsous2Component } from './prodsous2.component';

describe('Prodsous2Component', () => {
  let component: Prodsous2Component;
  let fixture: ComponentFixture<Prodsous2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Prodsous2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Prodsous2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
