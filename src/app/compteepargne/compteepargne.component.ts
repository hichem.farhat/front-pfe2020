import { Component, OnInit } from '@angular/core';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { GestionCompteépargneService } from '../services/gestion-compteépargne.service';
import { GestionuserService } from '../services/gestionuser.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-compteepargne',
  templateUrl: './compteepargne.component.html',
  styleUrls: ['./compteepargne.component.css']
})
export class CompteepargneComponent implements OnInit {
  ce:any=[]
@LocalStorage() token:any
user:any
count:any
cars:any=[]
cat:any=[]
searchtext:any
p:number=1

  constructor(private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private serv:GestionCompteépargneService,private serv2:GestionuserService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }

  ngOnInit() {
    this.getuser()
    this.getallcepargne()
    this.getcount()
    this.getallcarac()
    this.getALLcAt()
  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  getuser(){
    this.serv2.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcepargne(){this.serv.getallCompteépargne().subscribe(
    data=>{this.ce=data},
erre=>{}

  )
}
supprimer(id:number){
  if (window.confirm('Are sure you want to delete this Abonnee ?')){
  this.serv.deleteCompteépargne(id).subscribe(
    data => {
      console.log(data)
      this.toastr.success(' data successfully deleted!')
      this.ngOnInit() },
    err => { })

    }
  }
  deconnexion()
  {
    this.local.clear("token")
    this.route.navigate(['/login'])
  }
  getcount(){
    this.serv.count().subscribe(
      data=>{
        this.count=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  
}
