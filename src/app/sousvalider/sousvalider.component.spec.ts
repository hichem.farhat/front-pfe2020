import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SousvaliderComponent } from './sousvalider.component';

describe('SousvaliderComponent', () => {
  let component: SousvaliderComponent;
  let fixture: ComponentFixture<SousvaliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SousvaliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SousvaliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
