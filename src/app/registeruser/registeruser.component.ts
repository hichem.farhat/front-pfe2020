import { Component, OnInit } from '@angular/core';
import { GestionuserService } from '../services/gestionuser.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestionagenceService } from '../services/gestionagence.service';
import { User } from '../user';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-registeruser',
  templateUrl: './registeruser.component.html',
  styleUrls: ['./registeruser.component.css']
})
export class RegisteruserComponent implements OnInit {
  
  ag:any=[]
  users:User= new User();
  user:any
  cat:any=[]
  cars:any=[]
  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private serv:GestionuserService ,private serv2:GestionagenceService,private toastr: ToastrService,private route:Router ,private local:LocalStorageService) { }

  ngOnInit() {
    this.getallagences()
    
    this.getuser()
    this.getALLcAt()
    this.getallcarac()
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  getallagences(){
    this.serv2.getallagence().subscribe(
      data=>{this.ag=data},
      erre=>{}
    )
  }
  add()
  {
  
  this.serv.save(this.users).subscribe(
    
  

    data=>{this.route.navigate(['/user'])
  
  this.ngOnInit()},
    erre=>{}
  )



}
deconnexion()
{
  this.local.clear("token")
  this.route.navigate(['/login'])
}

}
