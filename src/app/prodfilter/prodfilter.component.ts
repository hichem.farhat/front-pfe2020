import { Component, OnInit } from '@angular/core';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { ProduitService } from '../services/produit.service';
import { GestiontypeprodService } from '../services/gestiontypeprod.service';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';

@Component({
  selector: 'app-prodfilter',
  templateUrl: './prodfilter.component.html',
  styleUrls: ['./prodfilter.component.css']
})
export class ProdfilterComponent implements OnInit {
  prods: any = [];
  typeprods: any = [];
  id:any
  @LocalStorage() token:any;
  user:any
  cat: any = [];
  cars: any = [];
  searchtext:any
  p:number=1
  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private ac:ActivatedRoute,private local:LocalStorageService,private serv1: GestiontypeprodService,private serv: ProduitService,private route:Router,private toastr:ToastrService) {
    this.id=this.ac.snapshot.params['id'];
   }

  ngOnInit() {
    this.getallprods()
    this.getallcarac()
    this.getALLcAt()
    this.getuser()
    
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  deconnexion()
  {
    this.local.clear("token")
    this.route.navigate(['/login'])
  }
  getallprods(){
    this.serv.getprodbyx(this.id).subscribe(    data => {
      this.prods = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );}
  getalltype(){
    this.serv1.getalltypeprod().subscribe(
      data=>{
        this.typeprods=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

}}
