import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Navprod2Component } from './navprod2.component';

describe('Navprod2Component', () => {
  let component: Navprod2Component;
  let fixture: ComponentFixture<Navprod2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Navprod2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Navprod2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
