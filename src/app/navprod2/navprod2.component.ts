import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-navprod2',
  templateUrl: './navprod2.component.html',
  styleUrls: ['./navprod2.component.css']
})
export class Navprod2Component implements OnInit {

  id:number
  constructor(private ac:ActivatedRoute,private route:Router) { 
    this.id=this.ac.snapshot.params['id'];
   }

  

  ngOnInit() {
    if(this.id!=null){
      this.route.navigate(["/prodfilter2/"+this.id]);
    }

  }
}
