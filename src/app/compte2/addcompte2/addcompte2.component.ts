import { Component, OnInit } from '@angular/core';
import { Compte } from 'src/app/compte';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { GestionuserService } from 'src/app/services/gestionuser.service';
import { GestioncaracterstiqueService } from 'src/app/services/gestioncaracterstique.service';
import { GestioncategorieService } from 'src/app/services/gestioncategorie.service';
import { GestioncompteService } from 'src/app/services/gestioncompte.service';
import { GestiontypecompteService } from 'src/app/services/gestiontypecompte.service';
import { GestionclientService } from 'src/app/services/gestionclient.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addcompte2',
  templateUrl: './addcompte2.component.html',
  styleUrls: ['./addcompte2.component.css']
})
export class Addcompte2Component implements OnInit {

  clt:any=[]
 
  cars:any=[]
  cat:any=[]
  user:any
   tcp: any = [];
 
   comptes:Compte= new Compte();
   @LocalStorage() token:any;
   @LocalStorage() id:any;
 
   constructor(private serv7:GestionuserService,private serv5:GestioncategorieService,private serv4:GestioncaracterstiqueService,private serv:GestioncompteService,private serv2:GestionclientService,private serv3:GestiontypecompteService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }
 
   ngOnInit() {this.getallcarac()
     this.getuser()
     this.getallcat()
    
     this.getclt();
     this.gettypes();
     
     
   }
   getallcarac() {
     this.serv4.getallcarac().subscribe(
         data => {
             this.cars = data;
           //  console.log('exected' + data);
         },
         err => {
             console.log(err);
         }
     );
 
 }
 
 getallcat() {
     this.serv5.getallcat().subscribe(
         data => {
             this.cat = data;
             
         },
         err => {
             console.log(err);
         }
     );
 
 }
 
 getuser() {
     this.serv7.getrole().subscribe(
         data => {
             this.user = data;
            // console.log('exected' + data);
         },
         err => {
             console.log(err);
         }
     );
 
 }
 
   getclt(){
     this.serv2.getclientbyagence(this.id).subscribe(
       data => {
         this.clt = data; console.log('exected' + data);
       },
       err => {
         console.log(err);
       }
       );
     }
  
     deconnexion() {
      this.local.clear("token")
      this.local.clear("id")
      this.route.navigate(['/login'])
  }
    
       add()
       {
       
       this.serv.addaccount(this.comptes).subscribe(
   
         data=>{this.route.navigate(['/compte2'])
       this.ngOnInit()},
         erre=>{}
       )
     
     
     
     }
     gettypes(){
       this.serv3.getalltypecompte().subscribe(
         data=>{this.tcp=data},
     erre=>{}
   
       )
 
     }
 
 }
 
