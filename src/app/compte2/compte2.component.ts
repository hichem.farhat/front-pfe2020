import { Component, OnInit } from '@angular/core';
import { GestioncompteService } from '../services/gestioncompte.service';
import { GestionclientService } from '../services/gestionclient.service';
import { GestiontypecompteService } from '../services/gestiontypecompte.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Compte } from '../compte';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestionuserService } from '../services/gestionuser.service';

@Component({
  selector: 'app-compte2',
  templateUrl: './compte2.component.html',
  styleUrls: ['./compte2.component.css']
})
export class Compte2Component implements OnInit {
  clt:any=[]
  cp: any = [];
  tcp: any = [];
  prodsous: any =[];
  comptes:Compte= new Compte();
  @LocalStorage() id:any;
  @LocalStorage() token:any;
  searchtext:any
p:number=1
cars:any=[]
user:any

  constructor(private serv6:GestionuserService
,    private carac:GestioncaracterstiqueService,private serv:GestioncompteService,private serv2:GestionclientService,private serv3:GestiontypecompteService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }

  ngOnInit() {
    this.getuser()
    this.getallcarac()
    this.getaccountag()
   
  
    
    
    
    
    
  }
  getaccountag(){
    this.serv.getallcomptebyagence(this.id).subscribe(
      data=>{
        this.cp=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.carac.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
 
  
      deconnexion()
    {
      this.local.clear("token")
      this.local.clear("id")
      this.route.navigate(['/login'])
    }
    supprimer(ids:number){
      if (window.confirm('Are sure you want to delete this Abonnee ?')){
      this.serv.deleteaccount(ids).subscribe(
        data => {
          console.log(data)
          this.toastr.success(' data successfully deleted!')
          this.ngOnInit() },
        err => { })
    
        }
      }
     
    
    
    
    }
   
