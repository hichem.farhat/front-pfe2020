import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Compte2Component } from './compte2.component';

describe('Compte2Component', () => {
  let component: Compte2Component;
  let fixture: ComponentFixture<Compte2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Compte2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Compte2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
