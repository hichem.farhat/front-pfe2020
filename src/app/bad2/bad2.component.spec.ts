import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Bad2Component } from './bad2.component';

describe('Bad2Component', () => {
  let component: Bad2Component;
  let fixture: ComponentFixture<Bad2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Bad2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Bad2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
