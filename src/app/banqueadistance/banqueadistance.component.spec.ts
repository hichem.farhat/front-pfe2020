import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BanqueadistanceComponent } from './banqueadistance.component';

describe('BanqueadistanceComponent', () => {
  let component: BanqueadistanceComponent;
  let fixture: ComponentFixture<BanqueadistanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BanqueadistanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BanqueadistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
