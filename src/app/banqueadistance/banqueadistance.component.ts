import { Component, OnInit } from '@angular/core';
import { GestionBanqueadistanceService } from '../services/gestion-banqueadistance.service';
import { GestionuserService } from '../services/gestionuser.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';

@Component({
  selector: 'app-banqueadistance',
  templateUrl: './banqueadistance.component.html',
  styleUrls: ['./banqueadistance.component.css']
})
export class BanqueadistanceComponent implements OnInit {
  bad:any=[]
@LocalStorage() token:any
user:any
count:any
cars:any=[]
cat:any=[]
searchtext:any
p:number=1

  constructor(private serv4:GestioncaracterstiqueService,private serv5:GestioncategorieService,private serv:GestionBanqueadistanceService,private serv2:GestionuserService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }

  ngOnInit() {
    this.getuser()
    this.getallcbad()
    this.getcount()
    this.getallcarac()
    this.getallcat()
  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}

getallcat() {
    this.serv5.getallcat().subscribe(
        data => {
            this.cat = data;
            
        },
        err => {
            console.log(err);
        }
    );

}

  getuser(){
    this.serv2.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcbad(){this.serv.getallbanqueadistance().subscribe(
    data=>{this.bad=data},
erre=>{}

  )
}
supprimer(id:number){
  if (window.confirm('Are sure you want to delete this Abonnee ?')){
  this.serv.deletebad(id).subscribe(
    data => {
      console.log(data)
      this.toastr.success(' data successfully deleted!')
      this.ngOnInit() },
    err => { })

    }
  }
  deconnexion()
  {
    this.local.clear("token")
    this.route.navigate(['/login'])
  }
  getcount(){
    this.serv.count().subscribe(
      data=>{
        this.count=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }

}