import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditouiComponent } from './creditoui.component';

describe('CreditouiComponent', () => {
  let component: CreditouiComponent;
  let fixture: ComponentFixture<CreditouiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditouiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditouiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
