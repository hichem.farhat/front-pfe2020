import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GestioncreditService } from '../services/gestioncredit.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';

@Component({
  selector: 'app-creditoui',
  templateUrl: './creditoui.component.html',
  styleUrls: ['./creditoui.component.css']
})
export class CreditouiComponent implements OnInit {
ui:any=[]
user:any
@LocalStorage() token :any
cat:any=[]
cars:any=[]
searchtext:any
p:number=1

  

  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private serv:GestioncreditService,private route:Router,private toastr: ToastrService,private local:LocalStorageService) { }

  ngOnInit(){
    this.getui()
    this.getuser()
    this.getALLcAt()
    this.getallcarac()
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  getui(){

    this.serv.getcreditui().subscribe(
        data=>{this.ui=data},
    erre=>{}
  
      )
    } deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
   


}
