import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { ClientComponent } from './client/client.component';
import { CategorieComponent } from './categorie/categorie.component';
import { AuthService } from './auth.service';

import { SouscriptionComponent } from './souscription/souscription.component';
import { TypeproduitsComponent } from './typeproduits/typeproduits.component';
import { EdittypeComponent } from './edittype/edittype.component';
import { ProduitsComponent } from './produits/produits.component';
import { CompteComponent } from './compte/compte.component';
import { ProdsousComponent } from './prodsous/prodsous.component';
import { AgenceComponent } from './agence/agence.component';
import { TypecompteComponent } from './typecompte/typecompte.component';
import { CreditComponent } from './credit/credit.component';
import { EditcltComponent } from './editclt/editclt.component';
import { EditcatComponent } from './editcat/editcat.component';
import { EditagComponent } from './editag/editag.component';
import { EdittcComponent } from './edittc/edittc.component';
import { EditaccountComponent } from './editaccount/editaccount.component';
import { EditprodComponent } from './editprod/editprod.component';
import { EditsousComponent } from './editsous/editsous.component';
import { ProdfilterComponent } from './prodfilter/prodfilter.component';
import { CltcatComponent } from './cltcat/cltcat.component';
import { NavComponent } from './nav/nav.component';
import { CaracteristiqueComponent } from './caracteristique/caracteristique.component';
import { EditcaracComponent } from './editcarac/editcarac.component';
import { CreditouiComponent } from './creditoui/creditoui.component';
import { CreditnonComponent } from './creditnon/creditnon.component';
import { CarteComponent } from './carte/carte.component';
import { ChequeComponent } from './cheque/cheque.component';
import { RegisteruserComponent } from './registeruser/registeruser.component';
import { Cl2Component } from './cl2/cl2.component';
import { Editcl2Component } from './editcl2/editcl2.component';
import { Compte2Component } from './compte2/compte2.component';
import { Editcompte2Component } from './editcompte2/editcompte2.component';
import { UserComponent } from './user/user.component';
import { Credit2Component } from './credit2/credit2.component';
import { Editcredit2Component } from './editcredit2/editcredit2.component';
import { EditcreditComponent } from './editcredit/editcredit.component';
import { Sous2Component } from './sous2/sous2.component';
import { Prodsous2Component } from './prodsous2/prodsous2.component';
import { Ce2Component } from './ce2/ce2.component';
import { Bad2Component } from './bad2/bad2.component';
import { Cav2Component } from './cav2/cav2.component';
import { Cheque2Component } from './cheque2/cheque2.component';
import { Carte2Component } from './carte2/carte2.component';
import { Ac2Component } from './ac2/ac2.component';
import { AddagComponent } from './agence/addag/addag.component';
import { AddcaracComponent } from './caracteristique/addcarac/addcarac.component';
import { AddcatComponent } from './categorie/addcat/addcat.component';
import { AddcltComponent } from './client/addclt/addclt.component';
import { AddcompteComponent } from './addcompte/addcompte.component';
import { EdituserComponent } from './user/edituser/edituser.component';
import { Banqueadistance } from './banqueadistance';
import { CompteAVueComponent } from './compte-a-vue/compte-a-vue.component';
import { CompteepargneComponent } from './compteepargne/compteepargne.component';
import { BanqueadistanceComponent } from './banqueadistance/banqueadistance.component';
import { Addcl2Component } from './cl2/addcl2/addcl2.component';
import { AddsousComponent } from './souscription/addsous/addsous.component';
import { Addcompte2Component } from './compte2/addcompte2/addcompte2.component';
import { Addcredit2Component } from './credit2/addcredit2/addcredit2.component';
import { Editsous2Component } from './editsous2/editsous2.component';
import { Addsous2Component } from './sous2/addsous2/addsous2.component';
import { SousvaliderComponent } from './sousvalider/sousvalider.component';
import { SousnonvaliderComponent } from './sousnonvalider/sousnonvalider.component';
import { BetweenComponent } from './between/between.component';
import { AddtcpComponent } from './typecompte/addtcp/addtcp.component';
import { Sousdate2Component } from './sous2/sousdate2/sousdate2.component';
import { Sousnonvalider2Component } from './sous2/sousnonvalider2/sousnonvalider2.component';
import { Sousvalider2Component } from './sous2/sousvalider2/sousvalider2.component';
import { Auth2Service } from './auth2.service';
import { Produits2Component } from './produits2/produits2.component';
import { Prodfilter2Component } from './prodfilter2/prodfilter2.component';
import { AddtpComponent } from './typeproduits/addtp/addtp.component';
import { AddcreditComponent } from './credit/addcredit/addcredit.component';
import { PdfComponent } from './pdf/pdf.component';
import { Nav22Component } from './nav22/nav22.component';
import { NavprodComponent } from './navprod/navprod.component';
import { Navprod2Component } from './navprod2/navprod2.component';
import { MoncompteComponent } from './moncompte/moncompte.component';
import { Moncompte2Component } from './moncompte2/moncompte2.component';
import { StatComponent } from './stat/stat.component';
import { Mdfpassword1Component } from './mdfpassword1/mdfpassword1.component';






const routes: Routes = [

  {path: '', component: LoginComponent},
  {path: 'prodfilter2/:id', component:Prodfilter2Component,canActivate:[Auth2Service] },
  {path: 'prod2', component: Produits2Component ,canActivate:[Auth2Service]},
  {path: 'sousvalider2', component: Sousvalider2Component ,canActivate:[AuthService]},
  {path: 'sousnonvalider2', component: Sousnonvalider2Component ,canActivate:[Auth2Service]},
  {path: 'sousdate2', component: Sousdate2Component ,canActivate:[Auth2Service]},
  {path: 'addcompte', component: AddcompteComponent ,canActivate:[AuthService]},
  {path: 'addcompte2', component: Addcompte2Component ,canActivate:[Auth2Service]},
  {path: 'addag', component: AddagComponent ,canActivate:[AuthService]},
  {path: 'addclt', component: AddcltComponent ,canActivate:[AuthService]},
  {path: 'addcat', component: AddcatComponent ,canActivate:[AuthService]},
  {path: 'addcarac', component: AddcaracComponent ,canActivate:[AuthService]},
  {path: 'login', component: LoginComponent},
  {path: 'client', component: ClientComponent ,canActivate:[AuthService]},
  {path: 'cat', component: CategorieComponent,canActivate:[AuthService]},
  {path: 'ac', component: AcceuilComponent,canActivate:[AuthService]},
  {path: 'ac2', component: Ac2Component,canActivate:[Auth2Service]},
  {path: 'compte', component: CompteComponent,canActivate:[AuthService]},
  {path: 'prodsous', component: ProdsousComponent,canActivate:[AuthService]},
  {path: 'sous', component: SouscriptionComponent,canActivate:[AuthService]},
  {path: 'type', component: TypeproduitsComponent,canActivate:[AuthService]},
  {path: 'edittype/:id', component: EdittypeComponent,canActivate:[AuthService]},
  {path: 'prod', component:ProduitsComponent,canActivate:[AuthService] },
  {path: 'ag', component:AgenceComponent,canActivate:[AuthService] },
  {path: 'tcpp', component:TypecompteComponent,canActivate:[AuthService] },
  {path: 'editclt/:id', component:EditcltComponent,canActivate:[AuthService] },
  {path: 'edituser/:id', component:EdituserComponent,canActivate:[AuthService] },
  {path: 'editcat/:id', component:EditcatComponent,canActivate:[AuthService] },
  {path: 'editag/:id', component:EditagComponent,canActivate:[AuthService] },
  {path: 'edittc/:id', component:EdittcComponent,canActivate:[AuthService] },
  {path: 'editacc/:id', component:EditaccountComponent,canActivate:[AuthService] },
  {path: 'editprod/:id', component:EditprodComponent,canActivate:[AuthService] },
  {path: 'editsous/:id', component:EditsousComponent,canActivate:[AuthService] },
  {path: 'editsous2/:id', component:Editsous2Component,canActivate:[Auth2Service] },
  {path: 'prodfilter/:id', component:ProdfilterComponent,canActivate:[AuthService] },
  {path: 'cltcat/:id', component:CltcatComponent },
  {path: 'nav', component:NavComponent,canActivate:[AuthService] },
  {path: 'carac', component:CaracteristiqueComponent,canActivate:[AuthService] },
  {path:'editcarac/:id',component:EditcaracComponent,canActivate:[AuthService]},
  {path:'creditoui',component:CreditouiComponent,canActivate:[AuthService]},
  {path:'creditnon',component:CreditnonComponent,canActivate:[AuthService]},
  {path:'carte',component:CarteComponent,canActivate:[AuthService]},
  {path:'cheque',component:ChequeComponent,canActivate:[AuthService]},
  {path:'register',component:RegisteruserComponent,canActivate:[AuthService]},
  {path: 'cl2', component: Cl2Component ,canActivate:[Auth2Service]},
  {path: 'editcl2/:id', component: Editcl2Component ,canActivate:[Auth2Service]},
  {path: 'compte2', component: Compte2Component ,canActivate:[Auth2Service]},
  {path: 'user', component: UserComponent ,canActivate:[AuthService]},
  {path: 'editcompte2/:id', component: Editcompte2Component ,canActivate:[Auth2Service]},
  {path: 'editcredit2/:id', component: Editcredit2Component ,canActivate:[Auth2Service]},
  {path: 'editcredit/:id', component: EditcreditComponent ,canActivate:[AuthService]},
  {path: 'credit2', component:Credit2Component,canActivate:[AuthService] },
  {path: 'sous2', component:Sous2Component,canActivate:[Auth2Service] },
  {path: 'prodsous2', component:Prodsous2Component,canActivate:[Auth2Service] },
  {path: 'ce2', component:Ce2Component,canActivate:[Auth2Service] },
  {path: 'bad2', component:Bad2Component,canActivate:[Auth2Service] },
  {path: 'cav2', component:Cav2Component,canActivate:[Auth2Service] },
  {path: 'cav', component:CompteAVueComponent,canActivate:[AuthService] },
  {path: 'ce', component:CompteepargneComponent,canActivate:[AuthService] },
  {path: 'bad', component:BanqueadistanceComponent,canActivate:[AuthService] },
  {path: 'addcl2', component:Addcl2Component,canActivate:[Auth2Service] },
  {path: 'addsous', component:AddsousComponent,canActivate:[AuthService] },
  {path: 'addcredit', component:AddcreditComponent,canActivate:[AuthService] },
  {path: 'addcredit2', component:Addcredit2Component,canActivate:[Auth2Service] },
  {path: 'cheque2', component:Cheque2Component,canActivate:[Auth2Service] },
  {path: 'addsous2', component:Addsous2Component,canActivate:[Auth2Service] },
  {path: 'carte2', component:Carte2Component,canActivate:[Auth2Service] },
  {path: 'sousvalider', component:SousvaliderComponent,canActivate:[AuthService] },
  {path: 'sousnonvalider', component:SousnonvaliderComponent,canActivate:[AuthService] },
  {path: 'between', component:BetweenComponent,canActivate:[AuthService] },
  {path: 'addtcp', component:AddtcpComponent,canActivate:[AuthService] },
  {path: 'addtp', component:AddtpComponent,canActivate:[AuthService] },
  {path: 'pdf', component:PdfComponent,canActivate:[AuthService] },
  {path: 'nav22/:id', component: Nav22Component },
  {path: 'navprod/:id', component: NavprodComponent },
  {path: 'navprod2/:id', component: Navprod2Component },
  {path: 'stat', component:StatComponent,canActivate:[AuthService] },
  {path: 'mdfpassword1', component:Mdfpassword1Component,canActivate:[AuthService] },
  {path: 'mdfpassword2', component:Mdfpassword1Component,canActivate:[Auth2Service] },

  {path: 'credit', component:CreditComponent,canActivate:[AuthService] },
  {path: 'moncompte', component:MoncompteComponent,canActivate:[AuthService] },
  {path: 'moncompte2', component:Moncompte2Component,canActivate:[Auth2Service] }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
