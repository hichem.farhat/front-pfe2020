import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestiontypeprodService } from '../services/gestiontypeprod.service';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-edittype',
  templateUrl: './edittype.component.html',
  styleUrls: ['./edittype.component.css']
})
export class EdittypeComponent implements OnInit {
  id:number;
  type=null
  message: string;
  user:any
  @LocalStorage() token :any
  cat:any=[]
  cars:any=[]
  
  

  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private ac:ActivatedRoute,private serv : GestiontypeprodService,private route:Router, private local:LocalStorageService) {
    this.id=this.ac.snapshot.params['id'];
   }

  ngOnInit(){
    this.gettypebyid()
    this.getuser()
    this.getALLcAt()
    this.getallcarac()
    
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  gettypebyid(){this.serv.gettypebyid(this.id).subscribe(
    data => { this.type=data
      console.log(data) },
    err => { console.log})}
    updateType() {
      this.serv.updatetype(this.type.id, this.type)
        .subscribe(
          data=>{this.route.navigate(['/type'])},
erre=>{}
);
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }

}
