import { Client } from './client'
import { Compte } from './compte'

export class Credit {
    id :number
    cl : Client
    montant :string
    cp:Compte
    date:string
    validation :string
    public set $validation(x: string) {
        this.validation = x;
        }
}
