import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { SouscriptionService } from '../services/souscription.service';
import { GestionclientService } from '../services/gestionclient.service';
import { GestioncompteService } from '../services/gestioncompte.service';
import { ProduitService } from '../services/produit.service';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { Souscription } from '../souscription';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-sous2',
  templateUrl: './sous2.component.html',
  styleUrls: ['./sous2.component.css']
})
export class Sous2Component implements OnInit {
  @LocalStorage() id:any
  @LocalStorage() token:any
  clt:any=[]
  prod:any=[]
  cp:any=[]
  user:any
  sous:any=[]
  searchtext:any
  p:number=1
  souss:Souscription= new Souscription();
  cars:any=[]
  constructor(private serv6:GestionuserService,private serv8:GestioncaracterstiqueService,private serv:SouscriptionService,private serv2:GestionclientService,private serv3:GestioncompteService,private serv4:ProduitService,private local:LocalStorageService,private route:Router,private toastr: ToastrService) { }

  ngOnInit() {
    this.getallclt()
    this.getallcompte()
    this.getallprod()
    this.getallsous()
    this.getuser()
    this.getallcarac()
  }

getallprod(){
    this.serv4.getallprod().subscribe(
      data => {
        this.prod = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }

  
  getallclt(){
    this.serv2.getclientbyagence(this.id).subscribe(
      data => {
        this.clt = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
  getallcompte(){
    this.serv3.getallcomptebyagence(this.id).subscribe(
      data => {
        this.cp = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
    getallsous(){
      this.serv.getallsousbyagence(this.id).subscribe(
        data => {
          this.sous = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
      }
      deconnexion()
      {
        this.local.clear("token")
        this.route.navigate(['/login'])
      }
      add()
      {
      
      this.serv.Savesous(this.souss).subscribe(
  
        data=>{this.route.navigate(['/sous2'])
      this.ngOnInit()},
        erre=>{}
      )
    
    
    
    }
    supprimer(id:number){
      if (window.confirm('Are sure you want to delete this Abonnee ?')){
      this.serv.delsous(id).subscribe(
        data => {
          console.log(data)
          this.toastr.success(' data successfully deleted!')
          this.ngOnInit() },
        err => { })
    
        }
      }
      getuser(){
        this.serv6.getrole().subscribe(
          data=>{
            this.user=data;console.log('exected' +data);
          },
          err=>{
            console.log(err);
          }
        );
    
      }
      getallcarac() {
        this.serv8.getallcarac().subscribe(
            data => {
                this.cars = data;
              //  console.log('exected' + data);
            },
            err => {
                console.log(err);
            }
        );
    
    }

}

