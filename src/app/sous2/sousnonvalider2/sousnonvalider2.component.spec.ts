import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Sousnonvalider2Component } from './sousnonvalider2.component';

describe('Sousnonvalider2Component', () => {
  let component: Sousnonvalider2Component;
  let fixture: ComponentFixture<Sousnonvalider2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Sousnonvalider2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Sousnonvalider2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
