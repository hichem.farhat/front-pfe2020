import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Sousdate2Component } from './sousdate2.component';

describe('Sousdate2Component', () => {
  let component: Sousdate2Component;
  let fixture: ComponentFixture<Sousdate2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Sousdate2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Sousdate2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
