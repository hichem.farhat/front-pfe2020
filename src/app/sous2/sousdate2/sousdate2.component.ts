import { Component, OnInit } from '@angular/core';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { GestioncategorieService } from 'src/app/services/gestioncategorie.service';
import { GestioncaracterstiqueService } from 'src/app/services/gestioncaracterstique.service';
import { SouscriptionService } from 'src/app/services/souscription.service';
import { GestionuserService } from 'src/app/services/gestionuser.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sousdate2',
  templateUrl: './sousdate2.component.html',
  styleUrls: ['./sousdate2.component.css']
})
export class Sousdate2Component implements OnInit {

  debut:any
fin :any
  @LocalStorage () token:any
  @LocalStorage () id:any
  user:any
  cars:any=[]
  cat:any=[]
  sous:any=[]
  constructor(private serv5: GestioncategorieService ,private serv8: GestioncaracterstiqueService ,private serv:SouscriptionService,private serv6: GestionuserService,private local:LocalStorageService,private route:Router,private toastr: ToastrService) { }

  ngOnInit() {
    this.getuser()
    this.getallsous()
    this.getallcarac()
    this.getALLcAt()
    this.getallsous()

  }
  
  getallsous(){
   
    this.serv.souscriptiondateag(this.debut,this.fin,this.id).subscribe(
      
      data => {
        console.log("debut"+this.debut)
        this.sous = data; 
        console.log('cccc' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
   
  
  getALLcAt(){
    this.serv5.getallcat().subscribe(
      data => {
        this.cat = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
    getuser(){
      this.serv6.getrole().subscribe(
        data=>{
          this.user=data;console.log('exected' +data);
        },
        err=>{
          console.log(err);
        }
      );
  
    }
    getallcarac() {
      this.serv8.getallcarac().subscribe(
          data => {
              this.cars = data;
            //  console.log('exected' + data);
          },
          err => {
              console.log(err);
          }
      );
  
  }

}