import { Component, OnInit } from '@angular/core';
import { SouscriptionService } from 'src/app/services/souscription.service';
import { GestionclientService } from 'src/app/services/gestionclient.service';
import { ProduitService } from 'src/app/services/produit.service';
import { GestioncompteService } from 'src/app/services/gestioncompte.service';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { GestionuserService } from 'src/app/services/gestionuser.service';
import { GestioncaracterstiqueService } from 'src/app/services/gestioncaracterstique.service';

@Component({
  selector: 'app-sousvalider2',
  templateUrl: './sousvalider2.component.html',
  styleUrls: ['./sousvalider2.component.css']
})
export class Sousvalider2Component implements OnInit {

  @LocalStorage() id:any
  @LocalStorage() token:any
  clt:any=[]
  prod:any=[]
  cp:any=[]
  sous:any=[]
  searchtext:any
  p:number=1
  cars:any=[]
  user:any
 
  constructor(private serv6:GestionuserService,private serv8:GestioncaracterstiqueService,private serv:SouscriptionService,private serv2:GestionclientService,private serv3:GestioncompteService,private serv4:ProduitService,private local:LocalStorageService,private route:Router,private toastr: ToastrService) { }

  ngOnInit() {
    this.getallcarac()
    this.getallclt()
    this.getallcompte()
    this.getallprod()
    this.getallsous()
    this.getuser()
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv8.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}


getallprod(){
    this.serv4.getallprod().subscribe(
      data => {
        this.prod = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }

  
  getallclt(){
    this.serv2.getclientbyagence(this.id).subscribe(
      data => {
        this.clt = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
  getallcompte(){
    this.serv3.getallcomptebyagence(this.id).subscribe(
      data => {
        this.cp = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
    getallsous(){
      this.serv.souscriptionvaliderag(this.id).subscribe(
        data => {
          this.sous = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
      }
      deconnexion()
      {
        this.local.clear("token")
        this.route.navigate(['/login'])
      }
     
    supprimer(id:number){
      if (window.confirm('Are sure you want to delete this Abonnee ?')){
      this.serv.delsous(id).subscribe(
        data => {
          console.log(data)
          this.toastr.success(' data successfully deleted!')
          this.ngOnInit() },
        err => { })
    
        }
      }}