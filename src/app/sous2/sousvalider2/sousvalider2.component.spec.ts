import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Sousvalider2Component } from './sousvalider2.component';

describe('Sousvalider2Component', () => {
  let component: Sousvalider2Component;
  let fixture: ComponentFixture<Sousvalider2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Sousvalider2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Sousvalider2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
