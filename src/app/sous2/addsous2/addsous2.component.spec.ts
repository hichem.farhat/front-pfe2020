import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Addsous2Component } from './addsous2.component';

describe('Addsous2Component', () => {
  let component: Addsous2Component;
  let fixture: ComponentFixture<Addsous2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Addsous2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Addsous2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
