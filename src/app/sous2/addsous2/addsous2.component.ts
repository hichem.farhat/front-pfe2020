import { Component, OnInit } from '@angular/core';
import { Souscription } from 'src/app/souscription';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { GestioncategorieService } from 'src/app/services/gestioncategorie.service';
import { GestioncaracterstiqueService } from 'src/app/services/gestioncaracterstique.service';
import { SouscriptionService } from 'src/app/services/souscription.service';
import { GestionclientService } from 'src/app/services/gestionclient.service';
import { GestioncompteService } from 'src/app/services/gestioncompte.service';
import { ProduitService } from 'src/app/services/produit.service';
import { GestionuserService } from 'src/app/services/gestionuser.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-addsous2',
  templateUrl: './addsous2.component.html',
  styleUrls: ['./addsous2.component.css']
})
export class Addsous2Component implements OnInit {

  clt:any=[]
  prod:any=[]
  cp:any=[]
  sous:any=[]
  souss:Souscription= new Souscription();
  cars:any=[]
  cat:any=[]
  user:any
  @LocalStorage () token:any
  
  
    constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv8: GestioncaracterstiqueService ,private serv:SouscriptionService,private serv2:GestionclientService,private serv3:GestioncompteService,private serv4:ProduitService,private local:LocalStorageService,private route:Router,private toastr: ToastrService) { }
  
    ngOnInit() {
      this.getallclt()
      this.getallcompte()
      this.getallprod()
      this.getallsous()
      this.getallcarac()
      this.getALLcAt()
      this.getuser()
    }
    getuser(){
      this.serv6.getrole().subscribe(
        data=>{
          this.user=data;console.log('exected' +data);
        },
        err=>{
          console.log(err);
        }
      );
  
    }
    getallcarac() {
      this.serv8.getallcarac().subscribe(
          data => {
              this.cars = data;
            //  console.log('exected' + data);
          },
          err => {
              console.log(err);
          }
      );
  
  }
  getALLcAt(){
    this.serv5.getallcat().subscribe(
      data => {
        this.cat = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
    getallprod(){
      this.serv4.getallprod().subscribe(
        data => {
          this.prod = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
      }
  
    
    getallclt(){
      this.serv2.getallclt().subscribe(
        data => {
          this.clt = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
      }
    getallcompte(){
      this.serv3.getallcomptes().subscribe(
        data => {
          this.cp = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
      }
      getallsous(){
        this.serv.getallsous().subscribe(
          data => {
            this.sous = data; console.log('exected' + data);
          },
          err => {
            console.log(err);
          }
          );
        }
        deconnexion()
        {
          this.local.clear("token")
          this.route.navigate(['/login'])
        }
        add()
        {
        
        this.serv.Savesous(this.souss).subscribe(
    
          data=>{this.route.navigate(['/sous2'])
        this.ngOnInit()},
          erre=>{}
        )
      
      
      
      }
     }
  