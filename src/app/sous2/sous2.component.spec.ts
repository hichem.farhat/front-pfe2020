import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Sous2Component } from './sous2.component';

describe('Sous2Component', () => {
  let component: Sous2Component;
  let fixture: ComponentFixture<Sous2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Sous2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Sous2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
