import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditnonComponent } from './creditnon.component';

describe('CreditnonComponent', () => {
  let component: CreditnonComponent;
  let fixture: ComponentFixture<CreditnonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditnonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditnonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
