import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { GestioncreditService } from '../services/gestioncredit.service';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestionuserService } from '../services/gestionuser.service';

@Component({
  selector: 'app-creditnon',
  templateUrl: './creditnon.component.html',
  styleUrls: ['./creditnon.component.css']
})
export class CreditnonComponent implements OnInit {
non:any=[]
cars:any=[]
cat:any=[]
user:any
searchtext:any
p:number=1
@LocalStorage() token :any
  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private serv:GestioncreditService,private route:Router,private toastr: ToastrService,private local:LocalStorageService) { }

  ngOnInit() {
    this.getnon()
    this.getALLcAt()
    this.getallcarac()
    this.getuser()
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  
  getnon(){

    this.serv.getcreditnon().subscribe(
        data=>{this.non=data},
    erre=>{}
  
      )
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }

}
