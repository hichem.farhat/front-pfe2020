import { Component, OnInit } from '@angular/core';
import { GestionService } from '../gestion.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { TypeCompte } from '../type-compte';
import { GestiontypecompteService } from '../services/gestiontypecompte.service';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-typecompte',
  templateUrl: './typecompte.component.html',
  styleUrls: ['./typecompte.component.css']
})
export class TypecompteComponent implements OnInit {
  tcp:any=[]
  @LocalStorage() token:any;
  tcpp:TypeCompte=new TypeCompte()
  cat:any=[]
  cars:any=[]
  user:any
  searchtext:any
  p:number=1
  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private serv:GestiontypecompteService,private route:Router,private local:LocalStorageService,private toastr: ToastrService ){ }

  ngOnInit() {
    this.getall()
    this.getuser()
    this.getALLcAt()
    this.getallcarac()
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  getall(){
    this.serv.getalltypecompte().subscribe(
      data => {
        this.tcp = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );



  }
  save(){
    this.serv.savetypecp(this.tcpp).subscribe(
      data=>{this.route.navigate(['/tcpp'])
      this.ngOnInit()},
        erre=>{}
      )
  }
delete (id){



  if (window.confirm('Are sure you want to delete this type ?')){
    this.serv.deletetypecp(id).subscribe(
      data => {
        console.log(data)
        this.toastr.success(' data successfully deleted!')
        this.ngOnInit() },
      err => { })
  
      }
    }  deconnexion()
    {
      this.local.clear("user")
      this.route.navigate(['/login'])
    }

}
