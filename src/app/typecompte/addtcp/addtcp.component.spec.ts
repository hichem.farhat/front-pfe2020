import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddtcpComponent } from './addtcp.component';

describe('AddtcpComponent', () => {
  let component: AddtcpComponent;
  let fixture: ComponentFixture<AddtcpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddtcpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddtcpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
