import { Component, OnInit } from '@angular/core';
import { GestionuserService } from 'src/app/services/gestionuser.service';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { GestioncategorieService } from 'src/app/services/gestioncategorie.service';
import { TypeCompte } from 'src/app/type-compte';
import { GestioncaracterstiqueService } from 'src/app/services/gestioncaracterstique.service';
import { GestiontypecompteService } from 'src/app/services/gestiontypecompte.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-addtcp',
  templateUrl: './addtcp.component.html',
  styleUrls: ['./addtcp.component.css']
})
export class AddtcpComponent implements OnInit {
  
  @LocalStorage() token:any;
  tcpp:TypeCompte=new TypeCompte()
  cat:any=[]
  cars:any=[]
  user:any
  
  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private serv:GestiontypecompteService,private route:Router,private local:LocalStorageService,private toastr: ToastrService ){ }

  ngOnInit() {
    
    this.getuser()
    this.getALLcAt()
    this.getallcarac()
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
 



  
  save(){
    this.serv.savetypecp(this.tcpp).subscribe(
      data=>{this.route.navigate(['/tcpp'])
      this.ngOnInit()},
        erre=>{}
      )
  }

      deconnexion()
    {
      this.local.clear("user")
      this.route.navigate(['/login'])
    }

}
