import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestionagenceService } from '../services/gestionagence.service';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-editag',
  templateUrl: './editag.component.html',
  styleUrls: ['./editag.component.css']
})
export class EditagComponent implements OnInit {
  id:number;
  @LocalStorage() token:any;
  ag=null
  message: string;
  cat:any=[]
cars:any=[]
user:any
  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private ac:ActivatedRoute,private serv:GestionagenceService,private toastr: ToastrService,private route:Router ,private local:LocalStorageService) { 
    this.id=this.ac.snapshot.params['id'];}

  ngOnInit(){
    this.gettagbyid()
    this.getuser()
    this.getALLcAt()
    this.getallcarac()
  }
    getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  
  gettagbyid(){this.serv.getagbyid(this.id).subscribe(
    data => { this.ag=data
      console.log(data) },
    err => { console.log})}
    updateag() {
      this.serv.updateag(this.ag.id, this.ag)
        .subscribe(
          data=>{this.route.navigate(['/ag'])},
erre=>{}
);
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }

}
