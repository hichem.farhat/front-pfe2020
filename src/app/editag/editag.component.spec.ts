import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditagComponent } from './editag.component';

describe('EditagComponent', () => {
  let component: EditagComponent;
  let fixture: ComponentFixture<EditagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
