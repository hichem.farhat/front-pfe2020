import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';

@Injectable({
  providedIn: 'root'
})
export class Auth2Service {

  constructor(private local:LocalStorageService, private route:Router) { }
  canActivate(){
  let token= localStorage.getItem("token")
  let id =this.local.retrieve("id")
  if(token && id){
  return true;
  }else{
  this.route.navigate(['/login']);
  return false;
  }
  }
  }