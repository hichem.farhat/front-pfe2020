import { Component, OnInit } from '@angular/core';

import { Agence } from '../agence';
import { Router } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestionagenceService } from '../services/gestionagence.service';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-agence',
  templateUrl: './agence.component.html',
  styleUrls: ['./agence.component.css']
})
export class AgenceComponent implements OnInit {
  ag:any=[]
  agence:Agence= new Agence();
   user:any;
   cars:any=[]
   @LocalStorage() token:any
   cat:any=[]
  searchtext:any
  p:number=1
  count:any

  constructor(private serv4:GestioncaracterstiqueService,private serv5:GestioncategorieService,private serv7:GestionuserService,private serv:GestionagenceService,private route:Router,private local:LocalStorageService) { }

  ngOnInit() {
    this.getall()
    this.getuser()
    this.getallcarac()
    this.getallcat()
    this.getcount()
  }
  getcount(){
    this.serv.getcount().subscribe(
      data => {
        this.count = data
       ;
        
         console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
  }
  getall(){
    this.serv.getallagence().subscribe(
      data => {
        this.ag = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );



  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}

getallcat() {
    this.serv5.getallcat().subscribe(
        data => {
            this.cat = data;
            
        },
        err => {
            console.log(err);
        }
    );

}

  getuser(){
    this.serv7.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
 
      delete(id)
      {
      this.serv.deleteag(id).subscribe(
        data=>{ this.ngOnInit()},
        erre=>{}
      )
      }
      deconnexion()
      {
        this.local.clear("token")
        this.route.navigate(['/login'])
      }

}
