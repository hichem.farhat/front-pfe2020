import { Component, OnInit } from '@angular/core';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestionagenceService } from 'src/app/services/gestionagence.service';
import { Router } from '@angular/router';
import { Agence } from 'src/app/agence';
import { GestionuserService } from 'src/app/services/gestionuser.service';
import { GestioncaracterstiqueService } from 'src/app/services/gestioncaracterstique.service';
import { GestioncategorieService } from 'src/app/services/gestioncategorie.service';

@Component({
  selector: 'app-addag',
  templateUrl: './addag.component.html',
  styleUrls: ['./addag.component.css']
})
export class AddagComponent implements OnInit {
  agence:Agence= new Agence();
  user:any;
  cars:any=[]
  cat:any=[]
  @LocalStorage() token:any
  constructor(private serv4:GestioncaracterstiqueService,private serv5:GestioncategorieService,private serv:GestionagenceService,private route:Router,private local:LocalStorageService,private serv7:GestionuserService) { }

  ngOnInit() {
    this.getallcat()
    this.getuser()
    this.getallcarac()
  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}

getallcat() {
    this.serv5.getallcat().subscribe(
        data => {
            this.cat = data;
            
        },
        err => {
            console.log(err);
        }
    );

}
  add(){
    this.serv.addag(this.agence).subscribe(
      data=>{this.route.navigate(['/ag'])
      this.ngOnInit()},
        
        erre=>{}
      )}
      getuser(){
        this.serv7.getrole().subscribe(
          data=>{
            this.user=data;console.log('exected' +data);
          },
          err=>{
            console.log(err);
          }
        );
    
      }  deconnexion()
      {
        this.local.clear("token")
        this.route.navigate(['/login'])
      }


}
