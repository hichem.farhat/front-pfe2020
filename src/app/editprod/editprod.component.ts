import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';

import { ProduitService } from '../services/produit.service';
import { GestiontypeprodService } from '../services/gestiontypeprod.service';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-editprod',
  templateUrl: './editprod.component.html',
  styleUrls: ['./editprod.component.css']
})
export class EditprodComponent implements OnInit {
  id:number;
 user:any
  produit=null
  typeprods: any = [];
  message: string;
  cat: any = [];
  cars: any = [];
  @LocalStorage() token:any;
  
  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv4: GestioncaracterstiqueService ,private ac:ActivatedRoute,private serv : ProduitService,private serv2:GestiontypeprodService,private route:Router, private local:LocalStorageService) { 
    this.id=this.ac.snapshot.params['id'];
  }
  ngOnInit() {
    this.getalltyppeprod()
    this.getprodid()
    this.getALLcAt()
    this.getuser()
    this.getallcarac()
   
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
 

 
 
  getalltyppeprod(){
   
      this.serv2.getalltypeprod().subscribe(
        data=>{
          this.typeprods=data;console.log('exected' +data);
        },
        err=>{
          console.log(err);
        }
      );


  }
  getprodid(){this.serv.getprodid(this.id).subscribe(
    data => { this.produit=data
      console.log(data) },
    err => { console.log})}
    updateprod() {
      this.serv.updateprod(this.produit.id, this.produit)
        .subscribe(
          data=>{this.route.navigate(['/prod'])},
erre=>{}
);

} deconnexion()
{
  this.local.clear("token")
  this.route.navigate(['/login'])
}}
