import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GestionService {

  constructor(private http:HttpClient) { }
  login(username:string,password:string){
    return this.http.get("http://127.0.0.1:8080/us/verif/"+username+"/"+password)
}
getallcat(){

  return this.http.get("http://127.0.0.1:8080/cat/allcats")
}
updatecat(id: number,data){
  return this.http.put("http://127.0.0.1:8080/cat/updatecat/"+id,data)

}
getcatbyid(id:number){
  return this.http.get("http://127.0.0.1:8080/cat/cat/"+id)
}
addcats(cat:any){
  return this.http.post("http://127.0.0.1:8080/cat/addcat",cat)
}
getallclt(){
return this.http.get("http://127.0.0.1:8080/clt/clients")

}
delclt(id:number){
  return this.http.delete('http://127.0.0.1:8080/clt/delclt/'+id)


}
getcltbyid(id:number){
  return this.http.get("http://127.0.0.1:8080/clt/clt/"+id)
}
updateclt(id: number,data){
  return this.http.put("http://127.0.0.1:8080/clt/updateclt/"+id,data)
}
addclt(clt:any){
  return this.http.post("http://127.0.0.1:8080/clt/addclt",clt)
}
deletecat(id:number){


  return this.http.delete('http://127.0.0.1:8080/cat/delcat/'+id)
}
countclt(){
  return this.http.get("http://127.0.0.1:8080/clt/count")
}
getalltypeprod(){

  return this.http.get("http://127.0.0.1:8080/typeprod/Typeprod")
}
addtype(type:any){
return this.http.post("http://127.0.0.1:8080/typeprod/addtype",type)

}deletetype(id:number){

  return this.http.delete('http://127.0.0.1:8080/typeprod/deltype/'+id)
}
gettypebyid(id:number){
  return this.http.get('http://127.0.0.1:8080/typeprod/typeprod/'+id)
}
updatetype(id: number,data){
  return this.http.put('http://127.0.0.1:8080/typeprod/typeprod/'+id,data);
}
getcounttypeprod(){
  return this.http.get("http://127.0.0.1:8080/typeprod/count")

}
getallprod(){
  return this.http.get("http://127.0.0.1:8080/prod/prods")
}
addprod(produit:any){
  return this.http.post("http://127.0.0.1:8080/prod/addprod",produit)
}
deleteprod(id:number){
  return this.http.delete("http://127.0.0.1:8080/prod/delprod/"+id)
}

getallcomptes(){
  return this.http.get("http://127.0.0.1:8080/compte/comptes")
}
getallprodsous(){
  return this.http.get("http://127.0.0.1:8080/prodsous/prodsous")
}
saveprodsous(prodsous:any){
  return this.http.post("http://127.0.0.1:8080/prodsous/save",prodsous)

}
getallsous(){
  return this.http.get("http://127.0.0.1:8080/sous/souss")

}
deleteaccount(id:number){
  return this.http.delete("http://127.0.0.1:8080/compte/delcompte/"+id)
}
addaccount(compte:any){
  return this.http.post("http://127.0.0.1:8080/compte/save",compte)

}
getalltypecompte(){
  return this.http.get("http://127.0.0.1:8080/typecompte/typecp")

}
savetypecp(type:any){
  return  this.http.post("http://127.0.0.1:8080/typecompte/save",type)
}
deletetypecp(id :number){
  return this.http.delete("http://127.0.0.1:8080/typecompte/delctype/"+id)
}
Savesous(Souscription:any){

  return this.http.post("http://127.0.0.1:8080/sous/add",Souscription)


}
delprodsous(id:number){
  return this.http.delete("http://127.0.0.1:8080/prodsous/delprodsous/"+id)
  
}
delsous(id:number){
  return this.http.delete("http://127.0.0.1:8080/sous/delsous"+id)
  
}
getallagence(){
  return this.http.get("http://127.0.0.1:8080/ag/agences")
}
getallcarac(){
  return this.http.get("http://127.0.0.1:8080/carac/cars")
}
addag(ag:any){
  return this.http.post("http://127.0.0.1:8080/ag/save",ag)
}
deleteag(id:number){
  return this.http.delete("http://127.0.0.1:8080/ag/delag/"+id)
}
getallcredit(){
  return this.http.get("http://127.0.0.1:8080/credit/Credits/")
}
delcredit(id:number){
  return this.http.delete("http://127.0.0.1:8080/credit/delcredit/"+id)
}
savecredit(credit:any){
  return this.http.post("http://127.0.0.1:8080/credit/save/",credit)

}
validation (id: number,data){
  return this.http.put('http://127.0.0.1:8080/credit/creditv/'+id,data);
}
getcreditbyid(id:number){
  return this.http.get('http://127.0.0.1:8080/credit/cred/'+id);}

  getagbyid(id:number){
    return this.http.get('http://127.0.0.1:8080/ag/ag/'+id)

  }
  updateag(id: number,data){
    return this.http.put("http://127.0.0.1:8080/ag/updateag/"+id,data)
  }
  gettypecompte(id:number){
    return this.http.get("http://127.0.0.1:8080/typecompte/gettype/"+id)
  }
  updatetcp(id:number,data){
    return this.http.put("http://127.0.0.1:8080/typecompte/updatetcp/"+id,data)
  }getaccountbyid(id:number){
    return this.http.get("http://127.0.0.1:8080/compte/getcompte/"+id)
  }
  updatecompte(id:number,data){
    return this.http.put("http://127.0.0.1:8080/compte/updatecp/"+id,data)
  }
  getprodid(id:number){
    return this.http.get("http://127.0.0.1:8080/prod/getprod/"+id)
  }
  updateprod(id:number,data){
    return this.http.put("http://127.0.0.1:8080/prod/updateprod/"+id,data)
  }
  getsous(id:number){
   return this.http.get("http://127.0.0.1:8080/sous/getsous/"+id)

  }
  updatesouss(id:number,data){
    return this.http.put("http://127.0.0.1:8080/sous/updatetsous/"+id,data)
  }
  getprodsous(id:number){
    return this.http.get("http://127.0.0.1:8080/prodsous/getprodsous/"+id)
  }
  updateprodsouss(id:number,data){
    return this.http.put("http://127.0.0.1:8080/prodsous/updateprodsous/"+id,data)
  }
  getprodbyx(id:number){
    return this.http.get("http://127.0.0.1:8080/prod/aze/"+id)
  }
  getallclientbycat(id:number){

    return this.http.get("http://127.0.0.1:8080/clt/cltcat/"+id)
  }
 



}
