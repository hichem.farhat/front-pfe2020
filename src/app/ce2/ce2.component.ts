import { Component, OnInit } from '@angular/core';
import { GestionCompteépargneService } from '../services/gestion-compteépargne.service';
import { ToastrService } from 'ngx-toastr';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-ce2',
  templateUrl: './ce2.component.html',
  styleUrls: ['./ce2.component.css']
})
export class Ce2Component implements OnInit {

  ce:any=[]
@LocalStorage() token:any
@LocalStorage() id:any
user:any
count:any
searchtext:any
p:number=1
cars:any=[]

  constructor(private serv:GestionCompteépargneService,private carac:GestioncaracterstiqueService
,    private serv2:GestionuserService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }

  ngOnInit() {
    this.getuser()
    this.getallcepargne()
    this.getcount()
    this.getallcarac()
  }
  getallcarac() {
    this.carac.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
  getuser(){
    this.serv2.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcepargne(){this.serv.getallCompteepargnebyagence(this.id).subscribe(
    data=>{this.ce=data},
erre=>{}

  )
}
supprimer(id:number){
  if (window.confirm('Are sure you want to delete this Abonnee ?')){
  this.serv.deleteCompteépargne(id).subscribe(
    data => {
      console.log(data)
      this.toastr.success(' data successfully deleted!')
      this.ngOnInit() },
    err => { })

    }
  }
  deconnexion() {
    this.local.clear("token")
    this.local.clear("id")
    this.route.navigate(['/login'])
}
  getcount(){
    this.serv.countag(this.id).subscribe(
      data=>{
        this.count=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  
}
