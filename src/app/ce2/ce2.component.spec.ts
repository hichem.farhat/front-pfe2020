import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Ce2Component } from './ce2.component';

describe('Ce2Component', () => {
  let component: Ce2Component;
  let fixture: ComponentFixture<Ce2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Ce2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Ce2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
