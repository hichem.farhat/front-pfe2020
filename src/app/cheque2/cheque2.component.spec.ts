import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Cheque2Component } from './cheque2.component';

describe('Cheque2Component', () => {
  let component: Cheque2Component;
  let fixture: ComponentFixture<Cheque2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Cheque2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Cheque2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
