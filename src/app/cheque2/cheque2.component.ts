import { Component, OnInit } from '@angular/core';
import { Cheque } from '../cheque';
import { GestionchequeService } from '../services/gestioncheque.service';
import { ToastrService } from 'ngx-toastr';
import { GestionuserService } from '../services/gestionuser.service';
import { Router } from '@angular/router';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-cheque2',
  templateUrl: './cheque2.component.html',
  styleUrls: ['./cheque2.component.css']
})
export class Cheque2Component implements OnInit {
  user:any
  cheque:any=[]
  ch:Cheque=new Cheque()
  count:any
  @LocalStorage() token :any
  @LocalStorage() id :any
  searchtext:any
  p:number=1
  cars:any=[]

  constructor(private carac: GestioncaracterstiqueService,private serv:GestionchequeService,private serv2:GestionuserService,private toastr: ToastrService,private route:Router, private local:LocalStorageService) { }

  ngOnInit() {
    this.getcount()
    this.getuser()
    this.getallcheque()
    this.getallcarac()
  }
  getallcarac() {
    this.carac.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
  getuser(){
    this.serv2.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcheque(){
    this.serv.getallchequebyagence(this.id).subscribe(
      data=>{this.cheque=data},
  erre=>{}

    )
  }
  add()
      {
      
      this.serv.addcheque(this.ch).subscribe(
  
        data=>{this.route.navigate(['/cheque'])
      this.ngOnInit()},
        erre=>{}
      )
    
    
    
    }
  supprimer(id:number){
    if (window.confirm('Are sure you want to delete this Abonnee ?')){
    this.serv.deletecheque(id).subscribe(
      data => {
        console.log(data)
        this.toastr.success(' data successfully deleted!')
        this.ngOnInit() },
      err => { })
  
      }
    }
    deconnexion() {
      this.local.clear("token")
      this.local.clear("id")
      this.route.navigate(['/login'])
  }
    getcount(){
      this.serv.countag(this.id).subscribe(
        data=>{
          this.count=data;console.log('exected' +data);
        },
        err=>{
          console.log(err);
        }
      );
  
    }

}
