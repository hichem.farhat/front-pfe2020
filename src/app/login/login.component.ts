import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { GestionuserService } from '../services/gestionuser.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

 error:any
  
  response:any={"token":""}
  
    constructor(private  serv:GestionuserService,private route:Router ,private local:LocalStorageService) { }
  
    ngOnInit() {
    }
    connexion(request){
      console.log(request.password)
      this.serv.login(request).subscribe(
      data => {
        
      this.response=data.body;
      let token=this.response.token;
      
      localStorage.setItem("token",token)
      

      //this.local.store("pass",request.password)
      this.serv.saveToken(token);
      
     
      this.route.navigate(['/nav']);

     
      },
      err => {
        this.error="login ou mot de passe incorrect"
        
      console.log(err)
     
      
      }
      )
      }
  }
  