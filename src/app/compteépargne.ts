import { ProdSous } from './prod-sous'
import { Compte } from './compte'
import { Client } from './client'

export class Compteépargne {
    id:number
    prods:ProdSous
    cp:Compte
    num:number
    cl:Client
}
