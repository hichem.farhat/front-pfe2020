import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Editsous2Component } from './editsous2.component';

describe('Editsous2Component', () => {
  let component: Editsous2Component;
  let fixture: ComponentFixture<Editsous2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Editsous2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Editsous2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
