import { Component, OnInit } from '@angular/core';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { SouscriptionService } from '../services/souscription.service';
import { GestionclientService } from '../services/gestionclient.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GestioncompteService } from '../services/gestioncompte.service';
import { ProduitService } from '../services/produit.service';

@Component({
  selector: 'app-editsous2',
  templateUrl: './editsous2.component.html',
  styleUrls: ['./editsous2.component.css']
})
export class Editsous2Component implements OnInit {

  id:number;
  souss=null
  message: string;
  clt:any=[]
  prod:any=[]
  cp:any=[]
  @LocalStorage() token:any;
  cat:any=[]
  cars:any=[]
  user:any

  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv9: GestioncaracterstiqueService ,private ac:ActivatedRoute,private serv1 : SouscriptionService,private serv2 : GestionclientService,private serv3: GestioncompteService,private serv4 : ProduitService,private route:Router, private local:LocalStorageService) {
    this.id=this.ac.snapshot.params['id'];
   }


  ngOnInit() {
    this.gettsoussbyid()
    this.getallprod()
    this.getallclt()
    this.getallclt()
    this.getallcarac()
    this.getuser()
    this.getALLcAt()

  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv9.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  gettsoussbyid(){this.serv1.getsous(this.id).subscribe(
    data => { this.souss=data
      console.log(data) },
    err => { console.log})}
    updatesous() {
      this.serv1.updatesouss(this.souss.id, this.souss)
        .subscribe(
          data=>{this.route.navigate(['/sous2'])},
erre=>{}
);
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
  getallprod(){
    this.serv4.getallprod().subscribe(
      data => {
        this.prod = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }

  
  getallclt(){
    this.serv2.getallclt().subscribe(
      data => {
        this.clt = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
  getallcompte(){
    this.serv3.getallcomptes().subscribe(
      data => {
        this.cp = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }

}

