import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Moncompte2Component } from './moncompte2.component';

describe('Moncompte2Component', () => {
  let component: Moncompte2Component;
  let fixture: ComponentFixture<Moncompte2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Moncompte2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Moncompte2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
