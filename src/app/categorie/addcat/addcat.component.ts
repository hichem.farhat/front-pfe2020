import { Component, OnInit } from '@angular/core';
import { GestionuserService } from 'src/app/services/gestionuser.service';
import { GestioncategorieService } from 'src/app/services/gestioncategorie.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Categorie } from 'src/app/categorie';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { GestioncaracterstiqueService } from 'src/app/services/gestioncaracterstique.service';

@Component({
  selector: 'app-addcat',
  templateUrl: './addcat.component.html',
  styleUrls: ['./addcat.component.css']
})
export class AddcatComponent implements OnInit {
 
    categorie:Categorie= new Categorie();
  @LocalStorage() token:any;
  user:any
  cars:any=[]
  cat:any=[]
    constructor(private serv4: GestioncaracterstiqueService,private serv7: GestionuserService,private serv1: GestioncategorieService,private route:Router,private toastr: ToastrService,private local:LocalStorageService) { }
  
    ngOnInit() {
      
      this.getuser()
      this.getALLcAt()
      this.getallcarac()
    }
   
    getallcarac() {
      this.serv4.getallcarac().subscribe(
          data => {
              this.cars = data;
            //  console.log('exected' + data);
          },
          err => {
              console.log(err);
          }
      );
  
  }
    getALLcAt(){
      this.serv1.getallcat().subscribe(
        data => {
          this.cat = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
      }
    getuser(){
      this.serv7.getrole().subscribe(
        data=>{
          this.user=data;console.log('exected' +data);
        },
        err=>{
          console.log(err);
        }
      );
  
    }
   
          add()
        {
        
        this.serv1.addcats(this.categorie).subscribe(
    
          data=>{this.route.navigate(['/cat'])
        this.ngOnInit()},
          erre=>{}
        )
      
      
      
  
      }
      deconnexion()
      {
        this.local.clear("token")
        this.route.navigate(['/login'])
      }
  
  
  
  
    
  
  }
  