import { Component, OnInit } from '@angular/core';

import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Categorie } from '../categorie';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestionuserService } from '../services/gestionuser.service';

@Component({
  selector: 'app-categorie',
  templateUrl: './categorie.component.html',
  styleUrls: ['./categorie.component.css']
})
export class CategorieComponent implements OnInit {
  cat: any = [];
  categorie:Categorie= new Categorie();
  @LocalStorage() token:any;
  cars: any = [];
  user:any
  searchtext:any
  p:number=1

  constructor(private serv2: GestionuserService,private serv4: GestioncaracterstiqueService ,private service: GestioncategorieService , private toastr: ToastrService,private route:Router,private local:LocalStorageService) { }

  ngOnInit() {
    this.getALLcAt();
    this.getallcarac()
    this.getuser()
  }
  getuser(){
    this.serv2.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
  getALLcAt(){
    this.service.getallcat().subscribe(
      data => {
        this.cat = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
    delete(id)
    {
    this.service.deletecat(id).subscribe(
      data=>{this.ngOnInit()},
      erre=>{}
    )
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
      add()
      {
      
      this.service.addcats(this.categorie).subscribe(

        data=>{this.route.navigate(['/cat'])
      this.ngOnInit()},
        
        erre=>{}
      )}}