import { Component, OnInit } from '@angular/core';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Caracteristique } from '../caracteristique';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';

@Component({
  selector: 'app-caracteristique',
  templateUrl: './caracteristique.component.html',
  styleUrls: ['./caracteristique.component.css']
})
export class CaracteristiqueComponent implements OnInit {
  cars:any=[]
  cat:any=[]
  user:any
  @LocalStorage() token :any
  searchtext:any
  p:number=1
  ca:Caracteristique= new Caracteristique();
  constructor(private serv5: GestioncategorieService,private serv7: GestionuserService,private serv1: GestioncaracterstiqueService,private route:Router,private toastr: ToastrService,private local:LocalStorageService) { }

  ngOnInit() {
    this.getallcarac()
    this.getuser()
    this.getallcat()
  }
  getallcat() {
    this.serv5.getallcat().subscribe(
        data => {
            this.cat = data;
            
        },
        err => {
            console.log(err);
        }
    );

}
  getuser(){
    this.serv7.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac(){
    this.serv1.getallcarac().subscribe(
      data=>{this.cars=data},
  erre=>{}

    )
  }
  deletecarac(id:number){
    if (window.confirm('Are sure you want to delete this Abonnee ?')){
      this.serv1.deletecarac(id).subscribe(
        data => {
          console.log(data)
          this.toastr.success(' data successfully deleted!')
          this.ngOnInit() },
        err => { })
    
        }}
        add()
      {
      
      this.serv1.add(this.ca).subscribe(
  
        data=>{this.route.navigate(['/carac'])
      this.ngOnInit()},
        erre=>{}
      )
    
    
    

    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }




  

}
