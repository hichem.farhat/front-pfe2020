import { Component, OnInit } from '@angular/core';
import { GestionuserService } from 'src/app/services/gestionuser.service';
import { GestioncaracterstiqueService } from 'src/app/services/gestioncaracterstique.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Caracteristique } from 'src/app/caracteristique';
import { GestioncategorieService } from 'src/app/services/gestioncategorie.service';

@Component({
  selector: 'app-addcarac',
  templateUrl: './addcarac.component.html',
  styleUrls: ['./addcarac.component.css']
})
export class AddcaracComponent implements OnInit {
  cars:any=[]
  @LocalStorage() token :any
  cat:any=[]
  user:any
  ca:Caracteristique= new Caracteristique();
  constructor(private serv4: GestioncaracterstiqueService,private serv5: GestioncategorieService,private serv7: GestionuserService,private serv1: GestioncaracterstiqueService,private route:Router,private toastr: ToastrService,private local:LocalStorageService) { }

  ngOnInit() {
  this.getuser()
this.getallcarac()
this.getallcat()}
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}

getallcat() {
    this.serv5.getallcat().subscribe(
        data => {
            this.cat = data;
            
        },
        err => {
            console.log(err);
        }
    );

}
  getuser(){
    this.serv7.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  
  
        add()
      {
      
      this.serv1.add(this.ca).subscribe(
  
        data=>{this.route.navigate(['/carac'])
      this.ngOnInit()},
        erre=>{}
      )
    
    
    

    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }


}
