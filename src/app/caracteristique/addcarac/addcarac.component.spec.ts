import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddcaracComponent } from './addcarac.component';

describe('AddcaracComponent', () => {
  let component: AddcaracComponent;
  let fixture: ComponentFixture<AddcaracComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddcaracComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddcaracComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
