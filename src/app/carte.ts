import { ProdSous } from './prod-sous'
import { Compte } from './compte'
import { Client } from './client'

export class Carte {
    id:number
    prods:ProdSous
    cp:Compte
    num:number
    cl:Client
}
