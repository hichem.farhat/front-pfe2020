import { Produit } from './produit'
import { Client } from './client'
import { Compte } from './compte'

export class Souscription {
    id :number
    libelle:string
    prod:Produit
    numSouscription:number
    cl: Client
    cp:Compte
    date:string
    etat:string

}
