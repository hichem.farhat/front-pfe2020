import { Component, OnInit } from '@angular/core';
import { GestionuserService } from '../services/gestionuser.service';
import { GestionagenceService } from '../services/gestionagence.service';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  @LocalStorage() token:any
  user:any=[]
  ag:any=[]
  cars:any=[]
  cat:any=[]
  searchtext:any
  p:number=1

  constructor(private serv4: GestioncaracterstiqueService , private serv5: GestioncategorieService ,private serv :GestionuserService,private serv2 :GestionagenceService,private route:Router,private local:LocalStorageService) { }

  ngOnInit(){
    
    this.getalluser()
    this.getallcarac()
    this.getALLcAt()
  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  getalluser(){
    this.serv.getuser().subscribe(    data => {
      this.user = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
 }
 getallagence(){
  this.serv2.getallagence().subscribe(    data => {
    this.ag = data; console.log('exected' + data);
  },
  err => {
    console.log(err);
  }
  );
  
}
delete(id)
      {
      this.serv.deleteuser(id).subscribe(
        data=>{this.getalluser()},
        erre=>{}
      )
      }
      deconnexion()
{
  this.local.clear("token")
  this.route.navigate(['/login'])
}





}
