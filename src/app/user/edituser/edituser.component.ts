import { Component, OnInit } from '@angular/core';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { ActivatedRoute, Router } from '@angular/router';
import { GestionuserService } from 'src/app/services/gestionuser.service';
import { GestionagenceService } from 'src/app/services/gestionagence.service';
import { GestioncaracterstiqueService } from 'src/app/services/gestioncaracterstique.service';
import { GestioncategorieService } from 'src/app/services/gestioncategorie.service';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {
  id:number;
  ag:any=[]
  user=null
  message: string;
  cars:any=[]
  cat:any=[]
  @LocalStorage()token:any
 
  constructor(private serv4: GestioncaracterstiqueService , private serv5: GestioncategorieService ,private ac:ActivatedRoute,private serv : GestionuserService,private serv2 : GestionagenceService,private route:Router, private local:LocalStorageService) { 
    this.id=this.ac.snapshot.params['id'];
  }

  ngOnInit() {
    this.getuser()
    this.getallagences() 
    this.getallcarac()
    this.getALLcAt()

  }
  getallcarac() {
    this.serv4.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  getuser(){
    this.serv.getuserr(this.id).subscribe(
      data => { this.user=data
        console.log(data) },
      err => { console.log})}
      updatuser() {
        this.serv.UPDATE(this.user.id, this.user)
          .subscribe(
            data=>{this.route.navigate(['/user'])},
  erre=>{}
  );
      }
      deconnexion()
      {
        this.local.clear("token")
        this.route.navigate(['/login'])
      }
      getallagences(){this.serv2.getallagence().subscribe(
        data => {
          this.ag = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
  
  
  
    }

}
