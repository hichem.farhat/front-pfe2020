import { Component, OnInit } from '@angular/core';
import { GestionService } from '../gestion.service';
import { LocalStorageService, LocalStorage } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { Souscription } from '../souscription';
import { ToastrService } from 'ngx-toastr';
import { ProdSous } from '../prod-sous';
import { SouscriptionService } from '../services/souscription.service';
import { ProduitService } from '../services/produit.service';
import { GestioncompteService } from '../services/gestioncompte.service';
import { GestionclientService } from '../services/gestionclient.service';
import { GestionuserService } from '../services/gestionuser.service';
import { GestioncategorieService } from '../services/gestioncategorie.service';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';


@Component({
  selector: 'app-souscription',
  templateUrl: './souscription.component.html',
  styleUrls: ['./souscription.component.css']
})
export class SouscriptionComponent implements OnInit {
clt:any=[]
prod:any=[]
cp:any=[]
sous:any=[]
souss:Souscription= new Souscription();
cars:any=[]
cat:any=[]
user:any
ss1:any
id:number
searchtext:any
p:number=1

  
  ss2:Souscription= new Souscription();
@LocalStorage () token:any


  constructor(private serv6: GestionuserService ,private serv5: GestioncategorieService ,private serv8: GestioncaracterstiqueService ,private serv:SouscriptionService,private serv2:GestionclientService,private serv3:GestioncompteService,private serv4:ProduitService,private local:LocalStorageService,private route:Router,private toastr: ToastrService) { }

  ngOnInit() {
    this.getallclt()
    this.getallcompte()
    this.getallprod()
    this.getallsous()
    this.getallcarac()
    this.getALLcAt()
    this.getuser()
    
  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.serv8.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
getALLcAt(){
  this.serv5.getallcat().subscribe(
    data => {
      this.cat = data; console.log('exected' + data);
    },
    err => {
      console.log(err);
    }
    );
  }
  getallprod(){
    this.serv4.getallprod().subscribe(
      data => {
        this.prod = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }

  
  getallclt(){
    this.serv2.getallclt().subscribe(
      data => {
        this.clt = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
  getallcompte(){
    this.serv3.getallcomptes().subscribe(
      data => {
        this.cp = data; console.log('exected' + data);
      },
      err => {
        console.log(err);
      }
      );
    }
    getallsous(){
      this.serv.getallsous().subscribe(
        data => {
          this.sous = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
      }
      deconnexion()
      {
        this.local.clear("token")
        this.route.navigate(['/login'])
      }
      add()
      {
      
      this.serv.Savesous(this.souss).subscribe(
  
        data=>{this.route.navigate(['/sous'])
      this.ngOnInit()},
        erre=>{}
      )
    
    
    
    }
    supprimer(id:number){
      if (window.confirm('Are sure you want to delete this Abonnee ?')){
      this.serv.delsous(id).subscribe(
        data => {
          console.log(data)
          this.toastr.success(' data successfully deleted!')
          this.ngOnInit() },
        err => { })
    
        }
      }
      getsousbyid(id){this.serv.getcountsouscriptionbyid(id).subscribe(
        data => { this.ss1=data
          console.log(data) },
        err => { console.log})}
       
        validation(id) {
        
          this.serv.valider(id,this.ss2 )
            .subscribe(
              data=>{this.ngOnInit()},
    erre=>{}
    );
        }
        az(id) {
        
          this.serv.refusé(id,this.ss2 )
            .subscribe(
              data=>{this.ngOnInit()},
    erre=>{}
    );
        }
}
