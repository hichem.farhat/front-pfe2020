import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddsousComponent } from './addsous.component';

describe('AddsousComponent', () => {
  let component: AddsousComponent;
  let fixture: ComponentFixture<AddsousComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddsousComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddsousComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
