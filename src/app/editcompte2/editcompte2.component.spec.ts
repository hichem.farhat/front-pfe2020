import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Editcompte2Component } from './editcompte2.component';

describe('Editcompte2Component', () => {
  let component: Editcompte2Component;
  let fixture: ComponentFixture<Editcompte2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Editcompte2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Editcompte2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
