import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GestiontypecompteService } from '../services/gestiontypecompte.service';
import { GestionclientService } from '../services/gestionclient.service';
import { GestioncompteService } from '../services/gestioncompte.service';
import { LocalStorage, LocalStorageService } from 'ngx-webstorage';
import { ToastrService } from 'ngx-toastr';
import { GestioncaracterstiqueService } from '../services/gestioncaracterstique.service';
import { GestionuserService } from '../services/gestionuser.service';

@Component({
  selector: 'app-editcompte2',
  templateUrl: './editcompte2.component.html',
  styleUrls: ['./editcompte2.component.css']
})
export class Editcompte2Component implements OnInit {
  @LocalStorage() token:any;
  @LocalStorage() id:any;
  ids:number;
  tcp: any = [];
  clt: any = [];
  comptes=null
  message: string;
  cars: any = [];
  user:any
  constructor(private serv6:GestionuserService
,    private carac:GestioncaracterstiqueService,private ac:ActivatedRoute,private serv1:GestioncompteService,private serv2:GestiontypecompteService,private serv3:GestionclientService,private toastr: ToastrService,private route:Router ,private local:LocalStorageService) { 
    this.ids=this.ac.snapshot.params['id'];
  }

  ngOnInit() {
    this.gettcomptebyid()
    this.getclt()
    this.gettypes()
    this.getuser()

  }
  getuser(){
    this.serv6.getrole().subscribe(
      data=>{
        this.user=data;console.log('exected' +data);
      },
      err=>{
        console.log(err);
      }
    );

  }
  getallcarac() {
    this.carac.getallcarac().subscribe(
        data => {
            this.cars = data;
          //  console.log('exected' + data);
        },
        err => {
            console.log(err);
        }
    );

}
  gettcomptebyid(){this.serv1.getaccountbyid(this.ids).subscribe(
    data => { this.comptes=data
      console.log(data) },
    err => { console.log})}
    updatecp() {
      this.serv1.updatecompte(this.comptes.id, this.comptes)
        .subscribe(
          data=>{this.route.navigate(['/compte2'])},
erre=>{}
);
    }
    deconnexion()
    {
      this.local.clear("token")
      this.route.navigate(['/login'])
    }
    getclt(){
      this.serv3.getclientbyagence(this.id).subscribe(
        data => {
          this.clt = data; console.log('exected' + data);
        },
        err => {
          console.log(err);
        }
        );
      }
      gettypes(){
        this.serv2.getalltypecompte().subscribe(
          data=>{this.tcp=data},
      erre=>{}
    
        )
  
      }

}
