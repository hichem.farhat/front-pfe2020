import { Client } from './client'
import { Compte } from './compte'
import { ProdSous } from './prod-sous'

export class CompteAVue {
    id:number
    prods:ProdSous
    cp:Compte
    num:number
    cl:Client
}
